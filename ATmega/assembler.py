"""
assembler.py - Opcode assembler for the ATmega package.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from instructions import Instruction

class AssemblerError(Exception):
    pass

class Assembler:

    """Assembles sequences of instructions to produce sequences of opcodes.
    
    Output can be obtained as a sequence of addresses and opcodes - see
    assemble(). HEX files can be produced by supplying the generated sequence
    to the write_hex() method.
    
    Alternatively, a sequence of instructions can be supplied to the
    print_listing() method to produce a program listing.
    """
    
    def __init__(self):
    
        pass
    
    def assemble(self, start_address, instruction_list):
    
        """This generator method iterates over a sequence of instructions,
        yielding a tuple containing the address and opcode for each of them.
        
        The address of each opcode is determined from the start address
        specified.
        """
        
        address = start_address
        labels = {}
        
        # Record labels.
        
        for instruction in instruction_list:
        
            if isinstance(instruction, Instruction):
                address += instruction.length()
            
            elif type(instruction) == type(Instruction):
                # A class was accidentally specified.
                raise AssemblerError, "Class specified instead of instance: %s" % instruction
            else:
                # A label string
                if labels.has_key(instruction):
                    raise AssemblerError, "Label already defined at %s: %s" % (hex(labels[instruction]), instruction)
                else:
                    labels[instruction] = address
        
        address = start_address
        
        for instruction in instruction_list:
        
            if isinstance(instruction, Instruction):
                opcode = instruction.generate(address, labels)
                yield (address, opcode)
                address += instruction.length()
    
    def checksum(self, record_data):
    
        """Returns the checksum for a record in a HEX file from the given string
        containing an ASCII representation of hexadecimal data.
        """
        
        checksum = 0
        i = 0
        while i < len(record_data):
        
            checksum += int(record_data[i:i+2], 16)
            i += 2
        
        return ("%02X" % (0x100 - (checksum & 0xff)))[-2:]
    
    def write_hex(self, assembly_list):
    
        """This generator method iterates over a sequence of tuples each
        containing an address and an opcode, yielding strings containing
        records for inclusion in a HEX file.
        """
        
        current_address = None
        current_record = ""
        
        for address, opcode in assembly_list:
        
            if current_address is None:
                current_address = address
            
            current_record += opcode.text().upper()
            
            if len(current_record) > 32:
            
                record_data = "%02x%04X00%s" % (len(current_record)/2, current_address, current_record)
                yield ":"+record_data+self.checksum(record_data)+"\r\n"
                current_address = None
                current_record = ""
        
        if current_record:
            record_data = "%02X%04X00%s" % (len(current_record)/2, current_address, current_record)
            yield ":"+record_data+self.checksum(record_data)+"\r\n"
        
        yield ":00000001FF\r\n"
    
    def print_listing(self, instruction_list, address = 0):
    
        """Prints a listing of the specified sequence of instructions to the
        console, using the given address as the address of the first instruction.
        """
        
        for instruction in instruction_list:
            if isinstance(instruction, Instruction):
                print "  %04x" % address, instruction
                address += instruction.length()
            else:
                # Print the label with a colon, following the usual convention.
                print
                print instruction+":"
