"""
instructions.py - Instruction definitions for the ATmega package.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from definitions import *

class ParseError(Exception):
    pass

class GenerationError(Exception):
    pass

class OpcodeTemplate:

    """Defines the binary format of an opcode for an instruction.
    
    The format of the text specified when a template is created is a sequence
    of characters that denote the purpose of each bit in the opcode, from the
    highest order bit to the lowest. Fixed bits are specified as 0 and 1
    characters. Other characters are used to specify fields within the opcode,
    typically as contiguous sequences of the same character.
    
    For example, the ADC instruction is defined to have an opcode template of
    000111rdddddrrrr, with the top six bits containing a fixed value. The other
    bits are used to specify fields containing the two registers used by the
    instruction.
    
    The charaters used to name fields correspond to those used in the Syntax
    definition for any given Instruction instance.
    """
    
    def __init__(self, text):
    
        self.text = text
        self.parse_opcode(text)
    
    def __repr__(self):
        return "<OpcodeTemplate '"+self.text+"'>"
    
    def length(self):
        """Returns the length of the opcode in bytes."""
        return len(self.text)/8
    
    def parse_opcode(self, text):
    
        s = len(text)
        pieces = []
        sizes = {}
        current_type = ""
        current = ""
        
        # Create a template that can be used to insert arguments into the
        # generated opcode.
        
        for c in text:
            if c not in current_type:
                if current:
                    pieces.append((current_type, s, current))
                    current = ""
                if c == "1" or c == "0":
                    current_type = "10"
                else:
                    current_type = c
            
            s -= 1
            sizes[current_type] = sizes.get(current_type, 0) + 1
            current += c
        
        if current:
            pieces.append((current_type, s, current))
        
        self.template = []
        self.lookup_mask = 0xffffffff
        self.lookup_value = 0
        
        for data_type, shift, digits in pieces:
        
            # Create a mask that isolates these digits from the original value.
            mask = ((1 << len(digits)) - 1)
            
            # Record the type, mask and number of places needed to shift a
            # value into place when constructing an opcode value.
            
            if data_type == "10":
                value = 0
                for digit in digits:
                    value = value << 1
                    if digit == "1":
                        value = value | 1
                
                self.template.append((value, mask, shift))
                self.lookup_value = self.lookup_value | (value << shift)
            else:
                # Calculate the amount these digits need to be shifted.
                remaining = sizes[data_type] - len(digits)
                
                # Record the remaining number of digits so that any more digits
                # for this type can be masked and shifted correctly.
                sizes[data_type] = remaining
                
                piece_shift = shift - remaining
                self.template.append((data_type, mask << remaining, piece_shift))
                self.lookup_mask = self.lookup_mask ^ (mask << shift)

class Opcode:

    """Represents an opcode containing a set of values for a specific instance
    of an instruction.
    """
    
    def __init__(self, value, length):
    
        self.value = value
        self.length = length
    
    def __repr__(self):
    
        format = "%%0%ix" % (self.length * 2)
        return "<Opcode '%s'>" % (format % self.value)
    
    def binary(self):
    
        value = self.value
        data = ""
        i = 0
        while i < self.length:
            pair = chr(value & 0xff)
            value = value >> 8
            pair += chr(value & 0xff)
            value = value >> 8
            
            # Put the higher order pairs of bytes into the string before the
            # lower order pairs. This results in the correct format for opcodes
            # that contain two 16-bit words.
            data = pair + data
            i += 2
        return data
    
    def text(self):
    
        value = self.value
        data = ""
        i = 0
        while i < self.length:
            pair = "%02x" % (value & 0xff)
            value = value >> 8
            pair += "%02x" % (value & 0xff)
            value = value >> 8
            
            data = pair + data
            i += 2
        return data

class Syntax:

    """Describes the syntax of an instruction as typically used in an assembly
    language program. In particular, the names and usage of registers and
    numeric constants are encoded in the syntax and these correspond to the
    names of the fields in the OpcodeTemplate for any given Instruction
    instance.
    
    The syntax of an instruction is a series of arguments separated by commas.
    Registers are specified using a combination of the R character and a lower
    case letter. Constants are specified using the letters, K, q, s and b.
    Addresses are specified using either the A or k character. The letters, X,
    Y and Z are used to specify the X, Y and Z registers.
    
    For example, the ANDI instruction has the syntax "Rd,K" which indicates
    that it requires a register and a constant value. The letter d indicates by
    convention that the register is used to hold result of the operation.
    
    More complex instructions use the colon character to indicate that two
    adjacent registers are used to provide a single value. The plus and minus
    characters are used in this context to indicate how these registers relate
    to each other.
    
    For example, the ADIW instruction has the syntax "Rd+1:Rd,K" which means
    that the instruction requires two adjacent registers, with the highest
    specified first, and a constant value. Since the corresponding opcode only
    contains the value of Rd, the value of Rd+1 is implied, but the syntax
    ensures that a suitable value is supplied in order to avoid ambiguity.
    
    Plus and minus characters are also used to specify offsets from addresses
    held in registers, as well as to indicate pre or post incrementing or
    decrementing of index register values.
    
    For example, one form of the STD instruction has the syntax "Y+q,Rr" which
    means that the contents of the register Rr is stored in the address given
    by the contents of the Y register plus the offset q.
    
    In another example, one form of the LPM instruction has the syntax "Rd,Z+"
    which means that the value given by the address stored in the Z register
    is stored in the register Rd and the address in the Z register is
    incremented afterwards.
    """
    
    def __init__(self, text):
    
        self.text = text
        self.parameters = self.parse_syntax(text)
    
    def __repr__(self):
    
        return "<Syntax: '"+self.text+"'>"
    
    def parse_syntax(self, text):
    
        pieces = []
        if not text:
            return pieces
        
        i = 0
        while i < len(text):
        
            j = i
            while j < len(text) and text[j] != ",":
                j += 1
            
            piece = text[i:j]
            
            if ":" in piece:
                piece = piece.split(":")
                register0 = self.parse_syntax(piece[0])
                register1 = self.parse_syntax(piece[1])
                pieces += register0 + [":"] + register1
            else:
                if piece[0] == "R":
                    pieces.append(Register(piece[1:]))
                else:
                    for c in piece:
                        if c in "-+":
                            pieces.append(c)
                        elif c == "X":
                            pieces.append(X())
                        elif c == "Y":
                            pieces.append(Y())
                        elif c == "Z":
                            pieces.append(Z())
                        elif c in "Kqsb":
                            pieces.append(Constant(c))
                        elif c in "Ak":
                            pieces.append(Address(c))
                        else:
                            pieces.append(c)
            
            if j < len(text):
                pieces.append(",")
                i = j + 1
            else:
                break
        
        return pieces

class Instruction:

    """Defines the name, syntax, opcode format and flag usage of an instruction.
    
    Specific instructions are defined as subclasses of this class, defining
    attributes for each of the components that describe the instruction.
    
    The text attribute contains the name of the instruction.
    The syntax attribute contains a Syntax object, describing how the
    instruction is invoked.
    The opcode attribute contains an OpcodeTemplate object, describing how the
    arguments to the instruction are stored in the bits of the opcode.
    The flags attribute contains a sequence of strings describing the CPU flags
    that are potentially affected by the instruction.
    """
    
    def __init__(self, *args, **kwargs):
    
        if kwargs.has_key("opcode"):
            self.insert_opcode_arguments(kwargs["opcode"])
        else:
            self.match_arguments(args)
        
        self.comment = kwargs.get("comment")
    
    def __repr__(self):
        if self.arguments_string:
            return "<"+self.text+" "+self.arguments_string+">"
        else:
            return "<"+self.text+">"
    
    def __str__(self):
        if self.arguments_string:
            text = self.text+" "+self.arguments_string
        else:
            text = self.text
        
        if self.comment:
            text += " ; "+self.comment
        return text
    
    def __eq__(self, other):
    
        return self.__class__ == other.__class__ and self.values == other.values
    
    def match_arguments(self, args):
    
        arguments = []
        arguments_string = ""
        values = {}
        
        i = 0
        for piece in self.syntax.parameters:
        
            if isinstance(piece, Operand):
                try:
                    argument = piece.validate(args[i])
                except IndexError:
                    raise ParseError, "Insuffient arguments to %s: %s" % (self.text, args)
                except ValidationError:
                    raise ParseError, "Invalid value for %s operand: %s" % (self.text, args[i])
                
                arguments_string += str(argument)
                
                if isinstance(argument, Register):
                    values[piece.name] = argument.name
                else:
                    values[piece.name] = argument
                i += 1
            else:
                arguments_string += piece
        
        if i < len(args):
            raise ParseError, "Too many arguments specified to %s: %s" % (self.text, args)
        
        self.arguments = arguments
        self.values = values
        self.arguments_string = arguments_string
    
    def insert_opcode_arguments(self, opcode):
    
        # Construct a values dictionary from the opcode specification.
        
        self.values = {}
        
        for data_type, mask, shift in self.opcode.template:
        
            if type(data_type) == str:
            
                # Put together operands that are split into pieces.
                value = self.values.setdefault(data_type, 0)
                self.values[data_type] = value | ((opcode >> shift) & mask)
        
        # Some argument fields in opcodes are not complete representations
        # of values. For example, instructions that accept registers 16-31
        # only contain four bits for the register number. So, to construct
        # the argument string, we call a specialised method which will
        # interpret the fields for each opcode.
        
        self.arguments = []
        self.arguments_string = ""
        
        for piece in self.syntax.parameters:
        
            if isinstance(piece, Operand):
                argument = self.argument_for_field(piece.name)
                self.arguments.append(argument)
                self.arguments_string += str(argument)
            else:
                self.arguments_string += piece
    
    def validate_arguments(self, values):
    
        pass
    
    def argument_for_field(self, name):
    
        if name == "X":
            return X()
        elif name == "Y":
            return Y()
        elif name == "Z":
            return Z()
        elif name == "d":
            return Register(self.values[name])
        elif name == "r":
            return Register(self.values[name])
        else:
            return self.values[name]
    
    def generate(self, address = 0, labels = {}):
    
        for key, value in self.values.items():
        
            if isinstance(value, Reference):
                try:
                    label_address = labels[value.name]
                except KeyError:
                    raise GenerationError, "Label not defined at %s: %s" % (hex(address), value.name)
                
                if not hasattr(self, "relative"):
                    reference_value = label_address
                elif self.relative:
                    reference_value = label_address - (address + 2)
                else:
                    reference_value = label_address
                
                self.values[key] = value.process(reference_value)
        
        # Validate on generation to allow Reference objects to be converted
        # to offsets at an earlier stage in the assembly process.
        # Note that this changes the values held in the instructions, so
        # validation can be performed once.
        self.validate_arguments(self.values)
        
        opcode = 0
        for data_type, mask, shift in self.opcode.template:
        
            if type(data_type) == str:
                value = self.values[data_type]
            else:
                value = data_type
            
            opcode = opcode | ((value & mask) << shift)
        
        return Opcode(opcode, self.length())
    
    def length(self):
        return self.opcode.length()

class InstructionLookup:

    """Relates integer values that represent opcodes to Instruction objects,
    enabling lookup and disassembly of binary program data.
    
    The Lookup instance in this module is initialised with the details of all
    the standard ATmega instructions, so this class does not normally need to
    be instantiated by developers.
    
    Initially, each instance of this class contains no information about
    opcodes and instructions. When an instruction is added to the class, a mask
    is constructed that can be used to remove non-invariant data from opcodes.
    Later lookups use this mask to determine if an opcode corresponds to one or
    more types of instruction.
    
    Note that overlapping definitions of instructions in the ATmega instruction
    set mean that more than one equally valid instruction may be found for a
    given opcode. When this occurs, the instructions returned should have the
    same effect, but it is not possible to know which would have been used in
    the original program without knowing the context in which it is used.
    """
    
    def __init__(self):
    
        self.mask_dict = {}
        self.name_dict = {}
    
    def add(self, opcode_template, instruction):
    
        mask = opcode_template.lookup_mask
        value = opcode_template.lookup_value
        
        value_dict = self.mask_dict.setdefault(mask, {})
        value_dict.setdefault(value, []).append(instruction)
        
        self.name_dict[instruction.text] = instruction
    
    def lookup(self, opcode, bytes = None):
    
        if bytes is None:
        
            bytes = 0
            i = opcode
            while i != 0:
                i = i >> 8
                bytes += 1
            
            if bytes % 2 != 0:
                bytes += 1
        
        possible = []
        
        for mask, values_dict in self.mask_dict.items():
        
            masked = opcode & mask
            
            try:
                for instruction_class in values_dict[masked]:
                
                    if bytes != instruction_class.opcode.length():
                        continue
                    
                    try:
                        instruction = instruction_class(opcode = opcode)
                        possible.append(instruction)
                    except ParseError:
                        pass
            
            except KeyError:
                pass
        
        if possible:
            return possible
        
        raise LookupError, "Failed to look up opcode: %x" % opcode
    
    def lookup_name(self, name):
    
        return self.name_dict[name]

Lookup = InstructionLookup()

class ADC(Instruction):

    text = "ADC"
    opcode = OpcodeTemplate("000111rdddddrrrr")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd,Rr")

Lookup.add(ADC.opcode, ADC)

class ADD(Instruction):

    text = "ADD"
    opcode = OpcodeTemplate("000011rdddddrrrr")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd,Rr")

Lookup.add(ADD.opcode, ADD)

class ADIW(Instruction):

    text = "ADIW"
    opcode = OpcodeTemplate("10010110KKddKKKK")
    flags = ("S", "V", "N", "Z", "C")
    syntax = Syntax("Rd+1:Rd,K")
    
    def validate_arguments(self, values):
    
        if not values["d"] in (24, 26, 28, 30):
            raise ValidationError, "Invalid register (Rd must be 24, 26, 27, 28) in %s: %i" % (self.text, values["d"])
        elif not values["d+1"] in (25, 27, 29, 31):
            raise ValidationError, "Invalid register (Rd+1 must be 25, 27, 29, 31) in %s: %i" % (self.text, values["d+1"])
        elif not 0 <= values["K"] <= 63:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 63) in %s: %i" % (self.text, values["K"])
        
        values["d"] = (values["d"] - 24) / 2
        values["d+1"] = (values["d+1"] - 24) / 2
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(24 + self.values[name] * 2)
        elif name == "d+1":
            return Register(25 + self.values[name] * 2)
        else:
            return self.values[name]

Lookup.add(ADIW.opcode, ADIW)

class AND(Instruction):

    text = "AND"
    opcode = OpcodeTemplate("001000rdddddrrrr")
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd,Rr")

Lookup.add(AND.opcode, AND)

class ANDI(Instruction):

    text = "ANDI"
    opcode = OpcodeTemplate("0111KKKKddddKKKK")
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd,K")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        elif not 0 <= values["K"] <= 255:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 255) in %s: %i" % (self.text, values["K"])
        
        values["d"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(ANDI.opcode, ANDI)

class ASR(Instruction):

    text = "ASR"
    opcode = OpcodeTemplate("1001010ddddd0101")
    flags = ("S", "V", "N", "Z", "C")
    syntax = Syntax("Rd")

Lookup.add(ASR.opcode, ASR)

class BCLR(Instruction):

    text = "BCLR"
    opcode = OpcodeTemplate("100101001sss1000")
    flags = ("I", "T", "H", "S", "V", "N", "Z", "C")
    syntax = Syntax("s")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["s"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= s <= 7) in %s: %i" % (self.text, values["s"])

Lookup.add(BCLR.opcode, BCLR)

class BLD(Instruction):

    text = "BLD"
    opcode = OpcodeTemplate("1111100ddddd0bbb")
    flags = ()
    syntax = Syntax("Rd,b")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["b"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= b <= 7) in %s: %i" % (self.text, values["b"])

Lookup.add(BLD.opcode, BLD)

class BRBC(Instruction):

    text = "BRBC"
    opcode = OpcodeTemplate("111101kkkkkkksss")
    flags = ()
    syntax = Syntax("s,k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not 0 <= values["s"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= s <= 7) in %s: %i" % (self.text, values["s"])
        elif not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRBC.opcode, BRBC)

class BRBS(Instruction):

    text = "BRBS"
    opcode = OpcodeTemplate("111100kkkkkkksss")
    flags = ()
    syntax = Syntax("s,k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not 0 <= values["s"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= s <= 7) in %s: %i" % (self.text, values["s"])
        elif not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRBS.opcode, BRBS)

class BRCC(Instruction):

    text = "BRCC"
    opcode = OpcodeTemplate("111101kkkkkkk000")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRCC.opcode, BRCC)

class BRCS(Instruction):

    text = "BRCS"
    opcode = OpcodeTemplate("111100kkkkkkk000")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRCS.opcode, BRCS)

class BREAK(Instruction):

    text = "BREAK"
    opcode = OpcodeTemplate("1001010110011000")
    flags = ()
    syntax = Syntax("")

Lookup.add(BREAK.opcode, BREAK)

class BREQ(Instruction):

    text = "BREQ"
    opcode = OpcodeTemplate("111100kkkkkkk001")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BREQ.opcode, BREQ)

class BRGE(Instruction):

    text = "BRGE"
    opcode = OpcodeTemplate("111101kkkkkkk100")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRGE.opcode, BRGE)

class BRHC(Instruction):

    text = "BRHC"
    opcode = OpcodeTemplate("111101kkkkkkk101")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRHC.opcode, BRHC)

class BRHS(Instruction):

    text = "BRHS"
    opcode = OpcodeTemplate("111100kkkkkkk101")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRHS.opcode, BRHS)

class BRID(Instruction):

    text = "BRID"
    opcode = OpcodeTemplate("111101kkkkkkk111")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRID.opcode, BRID)

class BRIE(Instruction):

    text = "BRIE"
    opcode = OpcodeTemplate("111100kkkkkkk111")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRIE.opcode, BRIE)

class BRLO(Instruction):

    text = "BRLO"
    opcode = OpcodeTemplate("111100kkkkkkk000")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRLO.opcode, BRLO)

class BRLT(Instruction):

    text = "BRLT"
    opcode = OpcodeTemplate("111100kkkkkkk100")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRLT.opcode, BRLT)

class BRMI(Instruction):

    text = "BRMI"
    opcode = OpcodeTemplate("111100kkkkkkk010")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRMI.opcode, BRMI)

class BRNE(Instruction):

    text = "BRNE"
    opcode = OpcodeTemplate("111101kkkkkkk001")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRNE.opcode, BRNE)

class BRPL(Instruction):

    text = "BRPL"
    opcode = OpcodeTemplate("111101kkkkkkk010")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRPL.opcode, BRPL)

class BRSH(Instruction):

    text = "BRSH"
    opcode = OpcodeTemplate("111101kkkkkkk000")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRSH.opcode, BRSH)

class BRTC(Instruction):

    text = "BRTC"
    opcode = OpcodeTemplate("111101kkkkkkk110")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRTC.opcode, BRTC)

class BRTS(Instruction):

    text = "BRTS"
    opcode = OpcodeTemplate("111100kkkkkkk110")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRTS.opcode, BRTS)

class BRVC(Instruction):

    text = "BRVC"
    opcode = OpcodeTemplate("111101kkkkkkk011")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRVC.opcode, BRVC)

class BRVS(Instruction):

    text = "BRVS"
    opcode = OpcodeTemplate("111100kkkkkkk011")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not (values["k"] % 2 == 0 and -128 <= values["k"] <= 126):
            raise ValidationError, "Invalid constant (must be -128 <= k <= 126 and even) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x40:
                return (self.values[name] - 128) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(BRVS.opcode, BRVS)

class BSET(Instruction):

    text = "BSET"
    opcode = OpcodeTemplate("100101000sss1000")
    flags = ("I", "T", "H", "S", "V", "N", "Z", "C")
    syntax = Syntax("s")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["s"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= s <= 7) in %s: %i" % (self.text, values["s"])

Lookup.add(BSET.opcode, BSET)

class BST(Instruction):

    text = "BST"
    opcode = OpcodeTemplate("1111101ddddd0bbb")
    flags = ("T",)
    syntax = Syntax("Rd,b")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["b"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= b <= 7) in %s: %i" % (self.text, values["b"])

Lookup.add(BST.opcode, BST)

class CALL(Instruction):

    text = "CALL"
    opcode = OpcodeTemplate("1001010kkkkk111k"+"kkkkkkkkkkkkkkkk")
    flags = ()
    syntax = Syntax("k")
    relative = False
    
    def validate_arguments(self, values):
    
        if not 0 <= values["k"] <= 8388606: # 8M - 2(also need to handle 64K - 1 case)
            raise ValidationError, "Invalid constant (must be 0 <= k <= 8388606) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(CALL.opcode, CALL)

class CBI(Instruction):

    text = "CBI"
    opcode = OpcodeTemplate("10011000AAAAAbbb")
    flags = ()
    syntax = Syntax("A,b")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["A"] <= 31:
            raise ValidationError, "Invalid address (must be 0 <= A <= 31) in %s: %i" % (self.text, values["A"])
        elif not 0 <= values["b"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= b <= 7) in %s: %i" % (self.text, values["b"])

Lookup.add(CBI.opcode, CBI)

class CBR(Instruction):

    text = "CBR"
    opcode = OpcodeTemplate("0111KKKKddddKKKK") # ANDI with bitwise !K
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd,K")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        elif not 0 <= values["K"] <= 255:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 255) in %s: %i" % (self.text, values["K"])
        
        values["d"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(CBR.opcode, CBR)

class CLC(Instruction):

    text = "CLC"
    opcode = OpcodeTemplate("1001010010001000")
    flags = ("C",)
    syntax = Syntax("")

Lookup.add(CLC.opcode, CLC)

class CLH(Instruction):

    text = "CLH"
    opcode = OpcodeTemplate("1001010011011000")
    flags = ("H",)
    syntax = Syntax("")

Lookup.add(CLH.opcode, CLH)

class CLI(Instruction):

    text = "CLI"
    opcode = OpcodeTemplate("1001010011111000")
    flags = ("I",)
    syntax = Syntax("")

Lookup.add(CLI.opcode, CLI)

class CLN(Instruction):

    text = "CLN"
    opcode = OpcodeTemplate("1001010010101000")
    flags = ("N",)
    syntax = Syntax("")

Lookup.add(CLN.opcode, CLN)

class CLR(Instruction):

    text = "CLR"
    opcode = OpcodeTemplate("001001dddddddddd")
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd")
    
    def argument_for_field(self, name):
    
        if name == "d":
            # Since this instruction is equivalent to EOR Rd,Rd the field must
            # contain a register number that can be broken up into two equal
            # register numbers in the same format as the opcode for EOR.
            eor = EOR(opcode = 0x2400 | self.values["d"])
            
            if eor.values["r"] != eor.values["d"]:
                raise ParseError, "Mismatched registers in opcode %s: %x, %x" % (self.text, eor.values["d"], eor.values["r"])
            
            # Return the value from the corresponding EOR instruction.
            return Register(eor.values["d"])
        else:
            return self.values[name]

Lookup.add(CLR.opcode, CLR)

class CLS(Instruction):

    text = "CLS"
    opcode = OpcodeTemplate("1001010011001000")
    flags = ("S",)
    syntax = Syntax("")

Lookup.add(CLS.opcode, CLS)

class CLT(Instruction):

    text = "CLT"
    opcode = OpcodeTemplate("1001010011101000")
    flags = ("T",)
    syntax = Syntax("")

Lookup.add(CLT.opcode, CLT)

class CLV(Instruction):

    text = "CLV"
    opcode = OpcodeTemplate("1001010010111000")
    flags = ("V",)
    syntax = Syntax("")

Lookup.add(CLV.opcode, CLV)

class CLZ(Instruction):

    text = "CLZ"
    opcode = OpcodeTemplate("1001010010011000")
    flags = ("Z",)
    syntax = Syntax("")

Lookup.add(CLZ.opcode, CLZ)

class COM(Instruction):

    text = "COM"
    opcode = OpcodeTemplate("1001010ddddd0000")
    flags = ("S", "V", "N", "Z", "C")
    syntax = Syntax("Rd")

Lookup.add(COM.opcode, COM)

class CP(Instruction):

    text = "CP"
    opcode = OpcodeTemplate("000101rdddddrrrr")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd,Rr")

Lookup.add(CP.opcode, CP)

class CPC(Instruction):

    text = "CPC"
    opcode = OpcodeTemplate("000001rdddddrrrr")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd,Rr")

Lookup.add(CPC.opcode, CPC)

class CPI(Instruction):

    text = "CPI"
    opcode = OpcodeTemplate("0011KKKKddddKKKK")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd,K")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        elif not 0 <= values["K"] <= 255:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 255) in %s: %i" % (self.text, values["K"])
        
        values["d"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(CPI.opcode, CPI)

class CPSE(Instruction):

    text = "CPSE"
    opcode = OpcodeTemplate("000100rdddddrrrr")
    flags = ()
    syntax = Syntax("Rd,Rr")

Lookup.add(CPSE.opcode, CPSE)

class DEC(Instruction):

    text = "DEC"
    opcode = OpcodeTemplate("1001010ddddd1010")
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd")

Lookup.add(DEC.opcode, DEC)

class DES(Instruction):

    text = "DES"
    opcode = OpcodeTemplate("10010100KKKK1011")
    flags = () # ?
    syntax = Syntax("K")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["K"] <= 15:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 15) in %s: %i" % (self.text, values["K"])

Lookup.add(DES.opcode, DES)

class EICALL(Instruction):

    text = "EICALL"
    opcode = OpcodeTemplate("1001010100011001")
    flags = ()
    syntax = Syntax("")

Lookup.add(EICALL.opcode, EICALL)

class EIJMP(Instruction):

    text = "EIJMP"
    opcode = OpcodeTemplate("1001010000011001")
    flags = ()
    syntax = Syntax("")

Lookup.add(EIJMP.opcode, EIJMP)

class ELPMi(Instruction):

    text = "ELPM"
    opcode = OpcodeTemplate("1001010111011000")
    flags = ()
    syntax = Syntax("") # R0

Lookup.add(ELPMi.opcode, ELPMi)

class ELPMii(Instruction):

    text = "ELPM"
    opcode = OpcodeTemplate("1001000ddddd0110")
    flags = ()
    syntax = Syntax("Rd,Z")

Lookup.add(ELPMii.opcode, ELPMii)

class ELPMiii(Instruction):

    text = "ELPM"
    opcode = OpcodeTemplate("1001000ddddd0111")
    flags = ()
    syntax = Syntax("Rd,Z+")

Lookup.add(ELPMiii.opcode, ELPMiii)

class EOR(Instruction):

    text = "EOR"
    opcode = OpcodeTemplate("001001rdddddrrrr")
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd,Rr")

Lookup.add(EOR.opcode, EOR)

class FMUL(Instruction):

    text = "FMUL"
    opcode = OpcodeTemplate("000000110ddd1rrr")
    flags = ("Z", "C")
    syntax = Syntax("Rd,Rr")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 23:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 23) in %s: %i" % (self.text, values["d"])
        elif not 16 <= values["r"] <= 23:
            raise ValidationError, "Invalid register (must be 16 <= Rr <= 23) in %s: %i" % (self.text, values["r"])
        
        values["d"] -= 16
        values["r"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d" or name == "r":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(FMUL.opcode, FMUL)

class FMULS(Instruction):

    text = "FMULS"
    opcode = OpcodeTemplate("000000111ddd0rrr")
    flags = ("Z", "C")
    syntax = Syntax("Rd,Rr")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 23:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 23) in %s: %i" % (self.text, values["d"])
        elif not 16 <= values["r"] <= 23:
            raise ValidationError, "Invalid register (must be 16 <= Rr <= 23) in %s: %i" % (self.text, values["r"])
        
        values["d"] -= 16
        values["r"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d" or name == "r":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(FMULS.opcode, FMULS)

class FMULSU(Instruction):

    text = "FMULSU"
    opcode = OpcodeTemplate("000000111ddd1rrr")
    flags = ("Z", "C")
    syntax = Syntax("Rd,Rr")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 23:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 23) in %s: %i" % (self.text, values["d"])
        elif not 16 <= values["r"] <= 23:
            raise ValidationError, "Invalid register (must be 16 <= Rr <= 23) in %s: %i" % (self.text, values["r"])
        
        values["d"] -= 16
        values["r"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d" or name == "r":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(FMULSU.opcode, FMULSU)

class ICALL(Instruction):

    text = "ICALL"
    opcode = OpcodeTemplate("1001010100001001")
    flags = ()
    syntax = Syntax("")

Lookup.add(ICALL.opcode, ICALL)

class IJMP(Instruction):

    text = "IJMP"
    opcode = OpcodeTemplate("1001010000001001")
    flags = ()
    syntax = Syntax("")

Lookup.add(IJMP.opcode, IJMP)

class IN(Instruction):

    text = "IN"
    opcode = OpcodeTemplate("10110AAdddddAAAA")
    flags = ()
    syntax = Syntax("Rd,A")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["A"] <= 63:
            raise ValidationError, "Invalid address (must be 0 <= A <= 63) in %s: %i" % (self.text, values["A"])

Lookup.add(IN.opcode, IN)

class INC(Instruction):

    text = "INC"
    opcode = OpcodeTemplate("1001010ddddd0011")
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd")

Lookup.add(INC.opcode, INC)

class JMP(Instruction):

    text = "JMP"
    opcode = OpcodeTemplate("1001010kkkkk110k"+"kkkkkkkkkkkkkkkk")
    flags = ()
    syntax = Syntax("k")
    relative = False
    
    def validate_arguments(self, values):
    
        if not 0 <= values["k"] <= 8388606: # 8M - 2
            raise ValidationError, "Invalid constant (must be 0 <= k <= 8388606) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(JMP.opcode, JMP)

class LAC(Instruction):

    text = "LAC"
    opcode = OpcodeTemplate("1001001ddddd0110")
    flags = () # ?
    syntax = Syntax("Z,Rd")

Lookup.add(LAC.opcode, LAC)

class LAS(Instruction):

    text = "LAS"
    opcode = OpcodeTemplate("1001001ddddd0101")
    flags = () # ?
    syntax = Syntax("Z,Rd")

Lookup.add(LAS.opcode, LAS)

class LAT(Instruction):

    text = "LAT"
    opcode = OpcodeTemplate("1001001ddddd0111")
    flags = () # ?
    syntax = Syntax("Z,Rd")

Lookup.add(LAT.opcode, LAT)

class LDi(Instruction):

    text = "LD"
    opcode = OpcodeTemplate("1001000ddddd1100")
    flags = ()
    syntax = Syntax("Rd,X")

Lookup.add(LDi.opcode, LDi)

class LDii(Instruction):

    text = "LD"
    opcode = OpcodeTemplate("1001000ddddd1101")
    flags = ()
    syntax = Syntax("Rd,X+")

Lookup.add(LDii.opcode, LDii)

class LDiii(Instruction):

    text = "LD"
    opcode = OpcodeTemplate("1001000ddddd1110")
    flags = ()
    syntax = Syntax("Rd,-X")

Lookup.add(LDiii.opcode, LDiii)

class LDDi(Instruction):

    text = "LD" # (LDD)
    opcode = OpcodeTemplate("1000000ddddd1000")
    flags = ()
    syntax = Syntax("Rd,Y")

Lookup.add(LDDi.opcode, LDDi)

class LDDii(Instruction):

    text = "LD" # (LDD)
    opcode = OpcodeTemplate("1001000ddddd1001")
    flags = ()
    syntax = Syntax("Rd,Y+")

Lookup.add(LDDii.opcode, LDDii)

class LDDiii(Instruction):

    text = "LD" # (LDD)
    opcode = OpcodeTemplate("1001000ddddd1010")
    flags = ()
    syntax = Syntax("Rd,-Y")

Lookup.add(LDDiii.opcode, LDDiii)

class LDDiv(Instruction):

    text = "LDD"
    opcode = OpcodeTemplate("10q0qq0ddddd1qqq")
    flags = ()
    syntax = Syntax("Rd,Y+q")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["q"] <= 63:
            raise ValidationError, "Invalid constant (must be 0 <= q <= 63) in %s: %i" % (self.text, values["q"])

Lookup.add(LDDiv.opcode, LDDiv)

class LDDzi(Instruction):

    text = "LD" # (LDD)
    opcode = OpcodeTemplate("1000000ddddd0000")
    flags = ()
    syntax = Syntax("Rd,Z")

Lookup.add(LDDzi.opcode, LDDzi)

class LDDzii(Instruction):

    text = "LD" # (LDD)
    opcode = OpcodeTemplate("1001000ddddd0001")
    flags = ()
    syntax = Syntax("Rd,Z+")

Lookup.add(LDDzii.opcode, LDDzii)

class LDDziii(Instruction):

    text = "LD" # (LDD)
    opcode = OpcodeTemplate("1001000ddddd0010")
    flags = ()
    syntax = Syntax("Rd,-Z")

Lookup.add(LDDziii.opcode, LDDziii)

class LDDziv(Instruction):

    text = "LDD"
    opcode = OpcodeTemplate("10q0qq0ddddd0qqq")
    flags = ()
    syntax = Syntax("Rd,Z+q")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["q"] <= 63:
            raise ValidationError, "Invalid constant (must be 0 <= q <= 63) in %s: %i" % (self.text, values["q"])

Lookup.add(LDDziv.opcode, LDDziv)

class LDI(Instruction):

    text = "LDI"
    opcode = OpcodeTemplate("1110KKKKddddKKKK")
    flags = ()
    syntax = Syntax("Rd,K")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        elif not 0 <= values["K"] <= 255:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 255) in %s: %i" % (self.text, values["K"])
        
        values["d"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(LDI.opcode, LDI)
    
class LDS(Instruction):

    text = "LDS"
    opcode = OpcodeTemplate("1001000ddddd0000"+"kkkkkkkkkkkkkkkk")
    flags = ()
    syntax = Syntax("Rd,k")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["k"] <= 65535:
            raise ValidationError, "Invalid constant (must be 0 <= k <= 65535) in %s: %i" % (self.text, values["k"])

Lookup.add(LDS.opcode, LDS)

class LDS16(Instruction):

    text = "LDS"
    opcode = OpcodeTemplate("10100kkkddddkkkk")
    flags = ()
    syntax = Syntax("Rd,k")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        elif not 0 <= values["k"] <= 127:
            raise ValidationError, "Invalid constant (must be 0 <= k <= 127) in %s: %i" % (self.text, values["k"])
        
        values["d"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(LDS16.opcode, LDS16)

class LPMi(Instruction):

    text = "LPM"
    opcode = OpcodeTemplate("1001010111001000")
    flags = ()
    syntax = Syntax("")

Lookup.add(LPMi.opcode, LPMi)

class LPMii(Instruction):

    text = "LPM"
    opcode = OpcodeTemplate("1001000ddddd0100")
    flags = ()
    syntax = Syntax("Rd,Z")

Lookup.add(LPMii.opcode, LPMii)

class LPMiii(Instruction):

    text = "LPM"
    opcode = OpcodeTemplate("1001000ddddd0101")
    flags = ()
    syntax = Syntax("Rd,Z+")

Lookup.add(LPMiii.opcode, LPMiii)

class LSL(Instruction):

    text = "LSL"
    opcode = OpcodeTemplate("000011dddddddddd")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd")

Lookup.add(LSL.opcode, LSL)

class LSR(Instruction):

    text = "LSR"
    opcode = OpcodeTemplate("1001010ddddd0110")
    flags = ("S", "V", "N", "Z", "C")
    syntax = Syntax("Rd")

Lookup.add(LSR.opcode, LSR)

class MOV(Instruction):

    text = "MOV"
    opcode = OpcodeTemplate("001011rdddddrrrr")
    flags = ()
    syntax = Syntax("Rd,Rr")

Lookup.add(MOV.opcode, MOV)

class MOVW(Instruction):

    text = "MOVW"
    opcode = OpcodeTemplate("00000001ddddrrrr")
    flags = ()
    syntax = Syntax("Rd+1:Rd,Rr+1:Rr")
    
    def validate_arguments(self, values):
    
        if not values["d"] % 2 == 0:
            raise ValidationError, "Invalid register (Rd must be 0, 2, ..., 30) in %s: %i" % (self.text, values["d"])
        elif not values["r"] % 2 == 0:
            raise ValidationError, "Invalid register (Rr must be 0, 2, ..., 30) in %s: %i" % (self.text, values["r"])
        
        values["d"] = values["d"] / 2
        values["d+1"] = values["d+1"] / 2
        values["r"] = values["r"] / 2
        values["r+1"] = values["r+1"] / 2
    
    def argument_for_field(self, name):
    
        if name == "d" or name == "r" or name == "d+1" or name == "r+1":
            return Register(self.values[name] * 2)
        else:
            return self.values[name]

Lookup.add(MOVW.opcode, MOVW)

class MUL(Instruction):

    text = "MUL"
    opcode = OpcodeTemplate("100111rdddddrrrr")
    flags = ("Z", "C")
    syntax = Syntax("Rd,Rr")

Lookup.add(MUL.opcode, MUL)

class MULS(Instruction):

    text = "MULS"
    opcode = OpcodeTemplate("00000010ddddrrrr")
    flags = ("Z", "C")
    syntax = Syntax("Rd,Rr")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        elif not 16 <= values["r"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rr <= 31) in %s: %i" % (self.text, values["r"])
        
        values["d"] -= 16
        values["r"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d" or name == "r":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(MULS.opcode, MULS)

class MULSU(Instruction):

    text = "MULSU"
    opcode = OpcodeTemplate("000000110ddd0rrr")
    flags = ("Z", "C")
    syntax = Syntax("Rd,Rr")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 23:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 23) in %s: %i" % (self.text, values["d"])
        elif not 16 <= values["r"] <= 23:
            raise ValidationError, "Invalid register (must be 16 <= Rr <= 23) in %s: %i" % (self.text, values["r"])
        
        values["d"] -= 16
        values["r"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d" or name == "r":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(MULSU.opcode, MULSU)

class NEG(Instruction):

    text = "NEG"
    opcode = OpcodeTemplate("1001010ddddd0001")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd")

Lookup.add(NEG.opcode, NEG)

class NOP(Instruction):

    text = "NOP"
    opcode = OpcodeTemplate("0000000000000000")
    flags = ()
    syntax = Syntax("")

Lookup.add(NOP.opcode, NOP)

class OR(Instruction):

    text = "OR"
    opcode = OpcodeTemplate("001010rdddddrrrr")
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd,Rr")

Lookup.add(OR.opcode, OR)

class ORI(Instruction):

    text = "ORI"
    opcode = OpcodeTemplate("0110KKKKddddKKKK")
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd,K")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        elif not 0 <= values["K"] <= 255:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 255) in %s: %i" % (self.text, values["K"])
        
        values["d"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(ORI.opcode, ORI)

class OUT(Instruction):

    text = "OUT"
    opcode = OpcodeTemplate("10111AArrrrrAAAA")
    flags = ()
    syntax = Syntax("A,Rr")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["A"] <= 63:
            raise ValidationError, "Invalid address (must be 0 <= A <= 63) in %s: %i" % (self.text, values["A"])

Lookup.add(OUT.opcode, OUT)

class POP(Instruction):

    text = "POP"
    opcode = OpcodeTemplate("1001000ddddd1111")
    flags = ()
    syntax = Syntax("Rd")

Lookup.add(POP.opcode, POP)

class PUSH(Instruction):

    text = "PUSH"
    opcode = OpcodeTemplate("1001001rrrrr1111")
    flags = ()
    syntax = Syntax("Rr")

Lookup.add(PUSH.opcode, PUSH)

class RCALL(Instruction):

    text = "RCALL"
    opcode = OpcodeTemplate("1101kkkkkkkkkkkk")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not -4096 <= values["k"] <= 4094:
            raise ValidationError, "Invalid constant (must be -4096 <= k <= 4094) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x800:
                return (self.values[name] - 0x1000) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(RCALL.opcode, RCALL)

class RET(Instruction):

    text = "RET"
    opcode = OpcodeTemplate("1001010100001000")
    flags = ()
    syntax = Syntax("")

Lookup.add(RET.opcode, RET)

class RETI(Instruction):

    text = "RETI"
    opcode = OpcodeTemplate("1001010100011000")
    flags = ("I",)
    syntax = Syntax("")

Lookup.add(RETI.opcode, RETI)

class RJMP(Instruction):

    text = "RJMP"
    opcode = OpcodeTemplate("1100kkkkkkkkkkkk")
    flags = ()
    syntax = Syntax("k")
    relative = True
    
    def validate_arguments(self, values):
    
        if not -4096 <= values["k"] <= 4094:
            raise ValidationError, "Invalid constant (must be -4096 <= k <= 4094) in %s: %i" % (self.text, values["k"])
        
        values["k"] = values["k"] / 2
    
    def argument_for_field(self, name):
    
        if name == "k":
            if self.values[name] & 0x800:
                return (self.values[name] - 0x1000) * 2
            else:
                return self.values[name] * 2
        else:
            return self.values[name]

Lookup.add(RJMP.opcode, RJMP)

class ROL(Instruction):

    text = "ROL"
    opcode = OpcodeTemplate("000111dddddddddd")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd")

Lookup.add(ROL.opcode, ROL)

class ROR(Instruction):

    text = "ROR"
    opcode = OpcodeTemplate("1001010ddddd0111")
    flags = ("S", "V", "N", "Z", "C")
    syntax = Syntax("Rd")

Lookup.add(ROR.opcode, ROR)

class SBC(Instruction):

    text = "SBC"
    opcode = OpcodeTemplate("000010rdddddrrrr")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd,Rr")

Lookup.add(SBC.opcode, SBC)

class SBCI(Instruction):

    text = "SBCI"
    opcode = OpcodeTemplate("0100KKKKddddKKKK")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd,K")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        elif not 0 <= values["K"] <= 255:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 255) in %s: %i" % (self.text, values["K"])
        
        values["d"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(SBCI.opcode, SBCI)

class SBI(Instruction):

    text = "SBI"
    opcode = OpcodeTemplate("10011010AAAAAbbb")
    flags = ()
    syntax = Syntax("A,b")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["A"] <= 31:
            raise ValidationError, "Invalid address (must be 0 <= A <= 31) in %s: %i" % (self.text, values["A"])
        elif not 0 <= values["b"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= b <= 7) in %s: %i" % (self.text, values["b"])

Lookup.add(SBI.opcode, SBI)

class SBIC(Instruction):

    text = "SBIC"
    opcode = OpcodeTemplate("10011001AAAAAbbb")
    flags = ()
    syntax = Syntax("A,b")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["A"] <= 31:
            raise ValidationError, "Invalid address (must be 0 <= A <= 31) in %s: %i" % (self.text, values["A"])
        elif not 0 <= values["b"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= b <= 7) in %s: %i" % (self.text, values["b"])

Lookup.add(SBIC.opcode, SBIC)

class SBIS(Instruction):

    text = "SBIS"
    opcode = OpcodeTemplate("10011011AAAAAbbb")
    flags = ()
    syntax = Syntax("A,b")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["A"] <= 31:
            raise ValidationError, "Invalid address (must be 0 <= A <= 31) in %s: %i" % (self.text, values["A"])
        elif not 0 <= values["b"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= b <= 7) in %s: %i" % (self.text, values["b"])

Lookup.add(SBIS.opcode, SBIS)

class SBIW(Instruction):

    text = "SBIW"
    opcode = OpcodeTemplate("10010111KKddKKKK")
    flags = ("S", "V", "N", "Z", "C")
    syntax = Syntax("Rd+1:Rd,K")
    
    def validate_arguments(self, values):
    
        if not values["d"] in (24, 26, 28, 30):
            raise ValidationError, "Invalid register (Rd must be 24, 26, 27, 28) in %s: %i" % (self.text, values["d"])
        elif not values["d+1"] in (25, 27, 29, 31):
            raise ValidationError, "Invalid register (Rd+1 must be 25, 27, 29, 31) in %s: %i" % (self.text, values["d+1"])
        elif not 0 <= values["K"] <= 63:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 63) in %s: %i" % (self.text, values["K"])

Lookup.add(SBIW.opcode, SBIW)

class SBR(Instruction):

    text = "SBR"
    opcode = OpcodeTemplate("0110KKKKddddKKKK")
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd,K")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        elif not 0 <= values["K"] <= 255:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 255) in %s: %i" % (self.text, values["K"])
        
        values["d"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(SBR.opcode, SBR)

class SBRC(Instruction):

    text = "SBRC"
    opcode = OpcodeTemplate("1111110rrrrr0bbb")
    flags = ()
    syntax = Syntax("Rr,b")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["b"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= b <= 7) in %s: %i" % (self.text, values["b"])

Lookup.add(SBRC.opcode, SBRC)

class SBRS(Instruction):

    text = "SBRS"
    opcode = OpcodeTemplate("1111111rrrrr0bbb")
    flags = ()
    syntax = Syntax("Rr,b")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["b"] <= 7:
            raise ValidationError, "Invalid bit (must be 0 <= b <= 7) in %s: %i" % (self.text, values["b"])

Lookup.add(SBRS.opcode, SBRS)

class SEC(Instruction):

    text = "SEC"
    opcode = OpcodeTemplate("1001010000001000")
    flags = ("C",)
    syntax = Syntax("")

Lookup.add(SEC.opcode, SEC)

class SEH(Instruction):

    text = "SEH"
    opcode = OpcodeTemplate("1001010001011000")
    flags = ("H",)
    syntax = Syntax("")

Lookup.add(SEH.opcode, SEH)

class SEI(Instruction):

    text = "SEI"
    opcode = OpcodeTemplate("1001010001111000")
    flags = ("I",)
    syntax = Syntax("")

Lookup.add(SEI.opcode, SEI)

class SEN(Instruction):

    text = "SEN"
    opcode = OpcodeTemplate("1001010000101000")
    flags = ("N",)
    syntax = Syntax("")

Lookup.add(SEN.opcode, SEN)

class SER(Instruction):

    text = "SER"
    opcode = OpcodeTemplate("11101111dddd1111")
    flags = ()
    syntax = Syntax("Rd")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        
        values["d"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(SER.opcode, SER)

class SES(Instruction):

    text = "SES"
    opcode = OpcodeTemplate("1001010001001000")
    flags = ("S",)
    syntax = Syntax("")

Lookup.add(SES.opcode, SES)

class SET(Instruction):

    text = "SET"
    opcode = OpcodeTemplate("1001010001101000")
    flags = ("T",)
    syntax = Syntax("")

Lookup.add(SET.opcode, SET)

class SEV(Instruction):

    text = "SEV"
    opcode = OpcodeTemplate("1001010000111000")
    flags = ("V",)
    syntax = Syntax("")

Lookup.add(SEV.opcode, SEV)

class SEZ(Instruction):

    text = "SEZ"
    opcode = OpcodeTemplate("1001010000011000")
    flags = ("Z",)
    syntax = Syntax("")

Lookup.add(SEZ.opcode, SEZ)

class SLEEP(Instruction):

    text = "SLEEP"
    opcode = OpcodeTemplate("1001010110001000")
    flags = ()
    syntax = Syntax("")

Lookup.add(SLEEP.opcode, SLEEP)

class SPM(Instruction):

    text = "SPM"
    opcode = OpcodeTemplate("1001010111101000")
    flags = ()
    syntax = Syntax("")

Lookup.add(SPM.opcode, SPM)

class SPM2i_iii(Instruction):

    text = "SPM"
    opcode = OpcodeTemplate("1001010111101000")
    flags = ()
    syntax = Syntax("")

Lookup.add(SPM2i_iii.opcode, SPM2i_iii)

class SPM2iv_vi(Instruction):

    text = "SPM"
    opcode = OpcodeTemplate("1001010111111000")
    flags = ()
    syntax = Syntax("Z+") # ?

Lookup.add(SPM2iv_vi.opcode, SPM2iv_vi)

class STi(Instruction):

    text = "ST"
    opcode = OpcodeTemplate("1001001rrrrr1100")
    flags = ()
    syntax = Syntax("X,Rr")

Lookup.add(STi.opcode, STi)

class STii(Instruction):

    text = "ST"
    opcode = OpcodeTemplate("1001001rrrrr1101")
    flags = ()
    syntax = Syntax("X+,Rr")

Lookup.add(STii.opcode, STii)

class STiii(Instruction):

    text = "ST"
    opcode = OpcodeTemplate("1001001rrrrr1110")
    flags = ()
    syntax = Syntax("-X,Rr")

Lookup.add(STiii.opcode, STiii)

class STDi(Instruction):

    text = "ST" # (STD)
    opcode = OpcodeTemplate("1000001rrrrr1000")
    flags = ()
    syntax = Syntax("Y,Rr")

Lookup.add(STDi.opcode, STDi)

class STDii(Instruction):

    text = "ST" # (STD)
    opcode = OpcodeTemplate("1001001rrrrr1001")
    flags = ()
    syntax = Syntax("Y+,Rr")

Lookup.add(STDii.opcode, STDii)

class STDiii(Instruction):

    text = "ST" # (STD)
    opcode = OpcodeTemplate("1001001rrrrr1010")
    flags = ()
    syntax = Syntax("-Y,Rr")

Lookup.add(STDiii.opcode, STDiii)

class STDiv(Instruction):

    text = "STD"
    opcode = OpcodeTemplate("10q0qq1rrrrr1qqq")
    flags = ()
    syntax = Syntax("Y+q,Rr")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["q"] <= 63:
            raise ValidationError, "Invalid constant (must be 0 <= q <= 63) in %s: %i" % (self.text, values["q"])

Lookup.add(STDiv.opcode, STDiv)

class STDzi(Instruction):

    text = "ST" # (STD)
    opcode = OpcodeTemplate("1000001rrrrr0000")
    flags = ()
    syntax = Syntax("Z,Rr")

Lookup.add(STDzi.opcode, STDzi)

class STDzii(Instruction):

    text = "ST" # (STD)
    opcode = OpcodeTemplate("1001001rrrrr0001")
    flags = ()
    syntax = Syntax("Z+,Rr")

Lookup.add(STDzii.opcode, STDzii)

class STDziii(Instruction):

    text = "ST" # (STD)
    opcode = OpcodeTemplate("1001001rrrrr0010")
    flags = ()
    syntax = Syntax("-Z,Rr")

Lookup.add(STDziii.opcode, STDziii)

class STDziv(Instruction):

    text = "STD"
    opcode = OpcodeTemplate("10q0qq1rrrrr0qqq")
    flags = ()
    syntax = Syntax("Z+q,Rr")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["q"] <= 63:
            raise ValidationError, "Invalid constant (must be 0 <= q <= 63) in %s: %i" % (self.text, values["q"])

Lookup.add(STDziv.opcode, STDziv)

class STS(Instruction):

    text = "STS"
    opcode = OpcodeTemplate("1001001rrrrr0000"+"kkkkkkkkkkkkkkkk")
    flags = ()
    syntax = Syntax("k,Rr")
    
    def validate_arguments(self, values):
    
        if not 0 <= values["k"] <= 65535:
            raise ValidationError, "Invalid constant (must be 0 <= k <= 65535) in %s: %i" % (self.text, values["k"])

Lookup.add(STS.opcode, STS)

class STS16(Instruction):

    text = "STS"
    opcode = OpcodeTemplate("10101kkkrrrrkkkk")
    flags = ()
    syntax = Syntax("k,Rr")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["r"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rr <= 31) in %s: %i" % (self.text, values["r"])
        elif not 0 <= values["k"] <= 127:
            raise ValidationError, "Invalid constant (must be 0 <= k <= 127) in %s: %i" % (self.text, values["k"])
        
        values["r"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "r":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(STS16.opcode, STS16)

class SUB(Instruction):

    text = "SUB"
    opcode = OpcodeTemplate("000110rdddddrrrr")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd,Rr")

Lookup.add(SUB.opcode, SUB)

class SUBI(Instruction):

    text = "SUBI"
    opcode = OpcodeTemplate("0101KKKKddddKKKK")
    flags = ("H", "S", "V", "N", "Z", "C")
    syntax = Syntax("Rd,K")
    
    def validate_arguments(self, values):
    
        if not 16 <= values["d"] <= 31:
            raise ValidationError, "Invalid register (must be 16 <= Rd <= 31) in %s: %i" % (self.text, values["d"])
        elif not 0 <= values["K"] <= 255:
            raise ValidationError, "Invalid constant (must be 0 <= K <= 255) in %s: %i" % (self.text, values["K"])
        
        values["d"] -= 16
    
    def argument_for_field(self, name):
    
        if name == "d":
            return Register(self.values[name] + 16)
        else:
            return self.values[name]

Lookup.add(SUBI.opcode, SUBI)

class SWAP(Instruction):

    text = "SWAP"
    opcode = OpcodeTemplate("1001010ddddd0010")
    flags = ()
    syntax = Syntax("Rd")

Lookup.add(SWAP.opcode, SWAP)

class TST(Instruction):

    text = "TST"
    opcode = OpcodeTemplate("001000dddddddddd")
    flags = ("S", "V", "N", "Z")
    syntax = Syntax("Rd")

Lookup.add(TST.opcode, TST)

class WDR(Instruction):

    text = "WDR"
    opcode = OpcodeTemplate("1001010110101000")
    flags = ()
    syntax = Syntax("")

Lookup.add(WDR.opcode, WDR)

class XCH(Instruction):

    text = "XCH"
    opcode = OpcodeTemplate("1001001ddddd0100")
    flags = () # ?
    syntax = Syntax("Z,Rd")

Lookup.add(XCH.opcode, XCH)
