"""
memory.py - Memory map definitions for the ATmega package.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import instructions

class VectorTableError(Exception):
    pass

tables = {
    "atmega168": ["RESET",          # 0x00
                  "INT0",           # 0x04
                  "INT1",           # 0x08
                  "PCINT0",         # 0x0c
                  "PCINT1",         # 0x10
                  "PCINT2",         # 0x14
                  "WDT",            # 0x18
                  "TIMER2 COMPA",   # 0x1c
                  "TIMER2 COMPB",   # 0x20
                  "TIMER2 OVF",     # 0x24
                  "TIMER1 CAPT",    # 0x28
                  "TIMER1 COMPA",   # 0x2c
                  "TIMER1 COMPB",   # 0x30
                  "TIMER1 OVF",     # 0x34
                  "TIMER0 COMPA",   # 0x38
                  "TIMER0 COMPB",   # 0x3c
                  "TIMER0 OVF",     # 0x40
                  "SPI, STC",       # 0x44
                  "USART, RX",      # 0x48
                  "USART, UDRE",    # 0x4c
                  "USART, TX",      # 0x50
                  "ADC",            # 0x54
                  "EE READY",       # 0x58
                  "ANALOG COMP",    # 0x5c
                  "TWI",            # 0x60
                  "SPM READY"       # 0x64
        ],
    
    "atmega32u2": ["RESET",         # 0x00,
                   "INT0",          # 0x04
                   "INT1",          # 0x08
                   "INT2",          # 0x0c
                   "INT3",          # 0x10
                   "INT4",          # 0x14
                   "INT5",          # 0x18
                   "INT6",          # 0x1c
                   "INT7",          # 0x20
                   "PCINT0",        # 0x24
                   "PCINT1",        # 0x28
                   "USB GENERAL",   # 0x2c
                   "USB ENDPOINT",  # 0x30
                   "WDT",           # 0x34
                   "TIMER1 CAPT",   # 0x38
                   "TIMER1 COMPA",  # 0x3c
                   "TIMER1 COMPB",  # 0x40
                   "TIMER1 COMPC",  # 0x44
                   "TIMER1 OVF",    # 0x48
                   "TIMER0 COMPA",  # 0x4c
                   "TIMER0 COMPB",  # 0x50
                   "TIMER0 OVF",    # 0x54
                   "SPI, STC",      # 0x58
                   "USART1 RX",     # 0x5c
                   "USART1 UDRE",   # 0x60
                   "USART1 TX",     # 0x64
                   "ANALOG COMP",   # 0x68
                   "EE READY",      # 0x6c
                   "SPM READY"      # 0x70
        ]
    }


class VectorTable:

    def __init__(self):
    
        self.entries = {}
    
    def add_routine(self, name, address_or_reference):
    
        self.entries[name.upper()] = address_or_reference
    
    def generate(self, device):
    
        try:
            table = tables[device.lower()]
        except KeyError:
            raise VectorTableError, "No information for device: %s" % device
        
        output = []
        
        for key in table:
        
            if self.entries.has_key(key):
                output += [instructions.RJMP(self.entries[key]),
                           instructions.RETI()]
            else:
                output += [instructions.RETI(), instructions.RETI()]
        
        return output
