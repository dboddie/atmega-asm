"""
definitions.py - Common definitions for the ATmega package.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class ValidationError(Exception):
    pass

class Operand:
    pass

class Register(Operand):

    """Describes a register in a CPU with a name and optional value.
    
    For general registers, the name refers to a register number from 0 to 31
    inclusive.
    """
    
    def __init__(self, name, value = None):
        self.name = name
        self.value = None
    
    def __repr__(self):
        return "<Register '%s'>" % self.name
    
    def __str__(self):
        return "R%s" % self.name
    
    def validate(self, register):
    
        # Only accept Register objects.
        
        if isinstance(register, Register):
            if 0 <= register.name <= 31:
                return register
            else:
                raise ValidationError, "Invalid register specified: %i" % register.name
        
        else:
            raise ValidationError, "Register expected but non-register specified: %s" % repr(register)

class Constant(Operand):

    """Describes a constant with a name and a value.
    
    The name is usually used to refer to the value in descriptions of
    instruction opcodes.
    """
    
    def __init__(self, name, value = None):
        self.name = name
        self.value = value
    
    def __repr__(self):
        if self.value:
            return "<Constant '"+self.name+" (%s)'>" % self.value
        else:
            return "<Constant '"+self.name+"'>"
    
    def __str__(self):
        return hex(self.value)
    
    def validate(self, value):
        return value

class Address(Operand):

    """Describes an address with a name and a value.
    
    The name is usually used to refer to the address in descriptions of
    instruction opcodes.
    """
    
    def __init__(self, name, value = None):
        self.name = name
        self.value = value
    
    def __repr__(self):
        if self.value:
            return "<Address '"+self.name+" (%s)'>" % hex(self.value)+"'>"
        else:
            return "<Address '"+self.name+"'>"
    
    def __str__(self):
        return hex(self.value)
    
    def validate(self, value):
        return value

class Status(Register):

    Flags = ("C", "Z", "N", "V", "S", "H", "T", "I")

class X(Register):

    """Describes the X index register with an optional value.
    """
    
    def __init__(self, value = None):
        self.name = "X" # 26 and 27
        self.value = value
    
    def __repr__(self):
        return "<Register 'X'>"
    
    def __str__(self):
        return "X"
    
    def validate(self, register):
    
        if isinstance(register, X):
            return register
        elif isinstance(register, Register):
            raise ValidationError, "Register X expected but invalid register specified: %s" % repr(register)
        else:
            raise ValidationError, "Register X expected but non-register specified: %s" % repr(register)

class Y(Register):

    """Describes the Y index register with an optional value.
    """
    
    def __init__(self, value = None):
        self.name = "Y" # 28 and 29
        self.value = value
    
    def __repr__(self):
        return "<Register 'Y'>"
    
    def __str__(self):
        return "Y"
    
    def validate(self, register):
    
        if isinstance(register, Y):
            return register
        elif isinstance(register, Register):
            raise ValidationError, "Register Y expected but invalid register specified: %s" % repr(register)
        else:
            raise ValidationError, "Register Y expected but non-register specified: %s" % repr(register)

class Z(Register):

    """Describes the Z index register with an optional value.
    """
    
    def __init__(self, value = None):
        self.name = "Z" # 30 and 31
        self.value = value
    
    def __repr__(self):
        return "<Register 'Z'>"
    
    def __str__(self):
        return "Z"
    
    def validate(self, register):
    
        if isinstance(register, Z):
            return register
        elif isinstance(register, Register):
            raise ValidationError, "Register Z expected but invalid register specified: %s" % repr(register)
        else:
            raise ValidationError, "Register Z expected but non-register specified: %s" % repr(register)

class IORegister(Register):
    pass

class RAMPX(IORegister):
    pass

class RAMPY(IORegister):
    pass

class RAMPZ(IORegister):
    pass

class RAMPD(IORegister):
    pass

class EIND(IORegister):
    pass

class SP(IORegister):

    def __init__(self, value = None):
        self.name = 0x3d # and 3e
        self.value = value
    
    def __repr__(self):
        return "<Register 'SP'>"
    
    def __str__(self):
        return "SP"

class Reference:

    """Describes a reference to a label in a sequence of instructions.
    
    The name specified by the reference should correspond to the name of a
    Label object that will be assembled in the sequence of instructions in
    which the reference occurs.
    
    References can be combined with certain arithmetic and bitwise operations.
    Operations are recorded until the process() method is called, at which time
    the operations are performed in sequence and the value returned.
    """
    
    def __init__(self, name):
        self.name = name
        self.operations = []
    
    def __repr__(self):
        return "<Reference '%s'>" % self.name
    
    def __eq__(self, other):
        return self.name == other.name
    
    def __str__(self):
        return self.name
    
    def __mod__(self, rhs):
        self.operations.append(("__mod__", rhs))
        return self
    
    def __rmod__(self, lhs):
        self.operations.append(("__rmod__", lhs))
        return self
    
    def __and__(self, rhs):
        self.operations.append(("__and__", rhs))
        return self
    
    def __rand__(self, lhs):
        self.operations.append(("__rand__", lhs))
        return self
    
    def __div__(self, rhs):
        self.operations.append(("__div__", rhs))
        return self
    
    def __rdiv__(self, lhs):
        self.operations.append(("__rdiv__", lhs))
        return self
    
    def __add__(self, rhs):
        self.operations.append(("__add__", rhs))
        return self
    
    def __radd__(self, lhs):
        self.operations.append(("__radd__", lhs))
        return self
    
    def __lshift__(self, rhs):
        self.operations.append(("__lshift__", rhs))
        return self
    
    def __rshift__(self, rhs):
        self.operations.append(("__rshift__", rhs))
        return self
    
    def process(self, value):
    
        for operation, operand in self.operations:
        
            value = getattr(value, operation)(operand)
        
        return value
