"""
disassembler.py - Opcode disassembler for the ATmega package.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from instructions import *

class DisassemblerError(Exception):
    pass

class Bytes:

    """Represents a sequence of bytes from a program.
    """
    
    def __init__(self, *values):
    
        self.values = values
    
    def __str__(self):
    
        return " ".join(map(hex, map(int, self.values)))

class Disassembler:

    """Disassembles sequences of opcodes to produce sequences of instructions.
    
    Data can be supplied either as a sequence of bytes - see disassemble() - or
    as a HEX file - see disassemble_hex().
    """
    
    def __init__(self):
    
        pass
    
    def disassemble(self, start_address, data):
    
        """This generator method iterates through the specified list of
        data, yielding a tuple containing address and opcode information for
        each opcode found.
        
        The addresses are calculated using the start_address specified.
        
        Note that the data must be supplied as a list of integers with values
        from 0 to 255, representing little endian encoded opcodes, and that the
        list will be emptied by this method.
        """
        
        address = start_address
        opcode = 0
        opcode_bytes = 0
        
        while data:
        
            # Construct longer opcodes by shifting earlier pairs of bytes to
            # the left and appending new pairs to the end of the number.
            # This results in a format that matches the specification of each
            # opcode.
            
            opcode = (opcode << 16) | data.pop(0) | (data.pop(0) << 8)
            opcode_bytes += 2
            
            try:
                possible = Lookup.lookup(opcode, opcode_bytes)
                
                if possible:
                    yield (address, ("%%0%ix" % (opcode_bytes*2)) % opcode, possible)
                    address += opcode_bytes
                    opcode = 0
                    opcode_bytes = 0
            
            except LookupError:
                pass
            
            if opcode_bytes == 4:
                # Write the first two unknown bytes as data.
                yield (address, "%04x" % ((opcode >> 16) & 0xffff), [Bytes((opcode >> 16) & 0xff, (opcode >> 24) & 0xff)])
                
                # Rewind two bytes.
                data = [opcode & 0xff, (opcode >> 8) & 0xff] + data
                address += 2
                opcode = 0
                opcode_bytes = 0
                
                #raise DisassemblerError, "Unrecognised opcode at %x" % address
    
    def read_hex(self, data):
    
        """This generator method reads the string data specified, iterating
        over the records it contains and yielding tuples describing them.
        
        Each tuple contains a number of items, beginning with a string
        containing the record type.
        
        Data records contain the string "Data", the address of the first byte
        of data they contain, and a list containing the bytes of data.
        
        Extended segment address records contain the string "Extended Segment
        Address" and the corresponding address.
        
        Extended linear address records contain the string "Extended Linear
        Address" and the corresponding address.
        
        End of file records contain only the string "End of File" and should
        occur once, at the end of the HEX file.
        """
        
        lines = data.split("\r\n")
        line_number = 1
        
        for line in lines:
        
            if not line:
                break
            
            start_code = line[0]
            if start_code != ":":
                raise DisassemblerError, "Colon expected at line %i of data" % line_number
            
            byte_count = int(line[1:3], 16)
            address = int(line[3:7], 16)
            record_type = int(line[7:9], 16)
            record_data = line[9:9 + byte_count * 2]
            
            if len(line) != 9 + byte_count * 2 + 2:
                raise DisassemblerError, "Incorrect record length at line %i of data" % line_number
            
            checksum = int(line[9 + byte_count * 2:], 16)
            
            # Check the data against the checksum.
            
            value = 0
            i = 1
            while i < 9 + byte_count * 2:
            
                value += int(line[i:i+2], 16)
                i += 2
            
            value = (0x100 - (value & 0xff)) & 0xff
            
            if value != checksum:
                raise DisassemblerError, "Data does not match checksum at line %i of data" % line_number
            
            # Create a record for the line.
            
            if record_type == 0:
            
                # Convert the pairs of characters into bytes.
                raw_data = []
                i = 0
                while i < len(record_data):
                    raw_data.append(int(record_data[i:i+2], 16))
                    i += 2
                
                yield ("Data", address, raw_data)
            
            elif record_type == 1:
                yield ("End of File",)
                break
            
            elif record_type == 2:
                assert(address == 0)
                assert(byte_count == 2)
                assert(record_data[-1] == "0")
                ext_address = int(record_data, 16)
                yield ("Extended Segment Address", ext_address)
            
            elif record_type == 3:
                #records.append(("Start Segment Address", address))
                pass
            
            elif record_type == 4:
                assert(address == 0)
                assert(byte_count == 2)
                ext_address = int(record_data, 16)
                yield ("Extended Linear Address", ext_address)
            
            elif record_type == 5:
                #records.append(("Start Linear Address", address))
                pass
            
            line_number += 1
    
    def disassemble_hex(self, hex_data):
    
        """This generator method iterates over the string specified by hex_data,
        yielding a tuple containing an address, opcode and instruction for each
        opcode found.
        """
        
        base_address = 0
        
        for record in self.read_hex(hex_data):
            
            if record[0] == "Data":
                for address, opcode_string, instruction in self.disassemble(base_address + record[1], record[2]):
                    yield (address, opcode_string, instruction)
            
            elif record[0] == "Extended Segment Address":
                base_address = record[1] << 4
            
            elif record[0] == "Extended Linear Address":
                base_address = (record[1] << 16) | (base_address & 0xffff)
