#!/usr/bin/env python

"""
disassemble.py - ATmega disassembler tool.

Copyright (C) 2011 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
from ATmega import disassembler

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s <HEX file>\n" % sys.argv[0])
        sys.exit(1)
    
    path = sys.argv[1]
    dis = disassembler.Disassembler()
    
    data = open(path, "rb").read()
    for address, opcode_string, instruction in dis.disassemble_hex(data):
    
        if isinstance(instruction, disassembler.Bytes):
            print "%04x  %s   %s" % (address, opcode_string, instruction[0])
        elif len(instruction) == 1:
            print "%04x  %s   %s" % (address, opcode_string, instruction[0])
        else:
            print "%04x  %s   %s ; %s" % (address, opcode_string, instruction[0], instruction[1:])
    
    sys.exit()
