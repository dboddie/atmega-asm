#!/usr/bin/env python

"""
upload.py - Upload a HEX file to an Arduino attached via a USB cable.

Copyright (C) 2011 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, sys
import pulse_serial

if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.stderr.write("Usage: %s <port> <baud rate> <HEX file>\n" % sys.argv[0])
        sys.stderr.write("Baud rate is usually 19200 bps.\n")
        sys.exit(1)
    
    try:
        port = sys.argv[1]
        baud_rate = sys.argv[2]
        path = sys.argv[3]
    
    except ValueError:
        sys.stderr.write("Please specify a duration in milliseconds.\n")
        sys.exit(1)
    
    pulse_serial.pulse(port, baud_rate, 100)
    
    sys.exit(os.system("avrdude -V -F -C /etc/avrdude.conf -p atmega168 -P %s -c stk500v1 -b %s -U flash:w:%s" % (port, baud_rate, path)))
