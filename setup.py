#!/usr/bin/env python

from distutils.core import setup

import ATmega

setup(
    name         = "ATmega",
    description  = "A package for assembling and disassembling ATmega code.",
    author       = "David Boddie",
    author_email = "david@boddie.org.uk",
    url          = "http://www.boddie.org.uk/david/Projects/Python/ATmega",
    version      = ATmega.__version__,
    packages     = ["ATmega"],
    scripts      = ["Tools/atmega-disassemble.py"]
    )
