=====================
ATmega Python Package
=====================

:Author: `David Boddie`_
:Date: 2012-03-16
:Version: 0.1.0

*Note: This text is marked up using reStructuredText formatting. It should be
readable in a text editor but can be processed to produce versions of this
document in other formats.*


.. contents::


Introduction
------------

The ATmega_ series of CPUs are a family of microcontrollers produced by Atmel_
with the AVR architecture. These 8-bit CPUs are used in the Arduino_ open
source electronics prototyping platform.


The ATmega Package
------------------

A Python_ package is provided that provides facilities for assembling and
disassembling code that can be run on CPUs in the ATmega series. Code is
assembled as a sequence of opcodes that can be serialised as program listings
or written out in Intel HEX format.

* The ``definitions`` module contains classes that describe features of the
  CPUs and helper classes for instruction validation and assembly.
* The ``instructions`` module contains classes that define the opcodes in the
  AVR instruction set, as well as classes for instruction parsing, validation
  and lookup.
* The ``assembler`` module provides a class with a convenient interface for
  assembly of sequences of instructions and creation of HEX files.
* The ``disassembler`` module provides a class with a convenient interface for
  disassembly of binary data containing opcodes and HEX files.


Requirements
------------

The ``pulse_serial.py`` tool depends on the pySerial_ module.


Installation
------------

To install the package alongside other packages and modules in your Python
installation, unpack the contents of the archive. At the command line, enter
the directory containing the ``setup.py`` script and install it by typing the
following::

  python setup.py install

You may need to become the root user or administrator to do this.


Tools
-----

Currently, the following tools are provided in the ``Tools`` directory.

* ``atmega-disassemble.py`` reads HEX files and displays listings of their
  contents.
* ``upload.py`` is used to send files to an Arduino board attached via a USB
  cable.
* ``pulse_serial.py`` is a helper module that is used by the ``upload.py``
  tool to reset the Arduino after a new program has been uploaded to it.


Tests
-----

The ``Tests`` directory contains simple test programs used at various times to
manually verify assembler output.


Examples
--------

The ``Examples`` directory contains example programs that demonstrate the use
of the modules in the package.


License
-------

The contents of this package are licensed under the GNU General Public License
(version 3 or later)::

 ATmega, a Python package for assembling and disassembling ATmega code.
 Copyright (C) 2012 David Boddie <david@boddie.org.uk>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.



.. _ATmega:         http://www.atmel.com/products/microcontrollers/avr/megaavr.aspx
.. _Atmel:          http://www.atmel.com
.. _Arduino:        http://arduino.cc
.. _Python:         http://www.python.org/
.. _pySerial:       http://pyserial.sf.net/
.. _`David Boddie`: mailto:david@boddie.org.uk
