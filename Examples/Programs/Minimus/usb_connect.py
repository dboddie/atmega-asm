#!/usr/bin/env python

"""
usb_connect.py - Example of assembler usage.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *
from ATmega.memory import *

R0 = Register(0)
R1 = Register(1)
R2 = Register(2)
R3 = Register(3)
R16 = Register(16)
R17 = Register(17)
R28 = Register(28)
R29 = Register(29)
R30 = Register(30)
R31 = Register(31)
X = X()
Y = Y()
Z = Z()

def setY(address):
    return [LDI(R28, address & 0xff),
            LDI(R29, address >> 8)]

def setZ(address):
    return [LDI(R30, address & 0xff),
            LDI(R31, address >> 8)]

def join(l):

    return reduce(lambda x, y: x + y, l)

# See section 11 of the ATmega32U2 datasheet.

vector_table = VectorTable()
vector_table.add_routine("RESET", Reference("init"))
vector_table.add_routine("USB GENERAL", Reference("usb_general"))

DDRD = 0x0a
DDD5 = 0x20 # 00100000
DDD6 = 0x40 # 01000000
LED_enable = DDD5 | DDD6

PORTD = 0x0b
blue_LED = 0x40
red_LED = 0x20

SREG = 0x3f

REGCR = 0x0063

USBCON = 0x00d8
USBE = 0x80

UDCON = 0x00e0
DETACH = 0x01
NOT_DETACH = 0xfe

UDINT = 0x00e1
EORSTI = 0x08
EORSTI_bit = 3
NOT_EORSTI = 0xf7
SOFI = 0x04
SOFI_bit = 2
NOT_SOFI = 0xfb

UDIEN = 0x00e2
EORSTE = 0x08

UDADDR = 0x00e3
ADDEN = 0x80
NOT_ADDEN = 0x7f

UEINTX = 0x00e8
RXSTPI = 0x08
RXSTPI_bit = 3
NOT_RXSTPI = 0xf7
TXINI = 0x01
NOT_TXINI = 0xfe

UENUM = 0x00e9
UECONX = 0x00eb
EPEN = 0x01
EPEN_bit = 0

UECFG0X = 0x00ec
TYPE_CONTROL = 0
DIRECTION_OUT = 0
DIRECTION_IN = 1

UECFG1X = 0x00ed
ALLOC = 0x02
SIZE_8 = 0x00
SIZE_16 = 0x10
SIZE_32 = 0x20
SIZE_64 = 0x30
ONE_BANK = 0x00
TWO_BANK = 0x04

UESTA0X = 0x00ee
CFGOK = 0x80
CFGOK_bit = 7

UEIENX = 0x00f0
RXSTPE = 0x08

UEDATX = 0x00f1

not_dict = {EORSTI_bit: NOT_EORSTI,
            SOFI_bit: NOT_SOFI,
            RXSTPI_bit: NOT_RXSTPI}

# USB constants

SETUP_SET_ADDRESS = 0x05
SETUP_GET_DESCRIPTOR = 0x06

USB_SETUP_DIR_HOST_TO_DEVICE = 0x00
USB_SETUP_DIR_DEVICE_TO_HOST = 0x80
USB_SETUP_TYPE_STANDARD = 0x00
USB_SETUP_TYPE_CLASS    = 0x20
USB_SETUP_TYPE_VENDOR   = 0x40
USB_SETUP_RECIPIENT_DEVICE    = 0x00
USB_SETUP_RECIPIENT_INTERFACE = 0x01
USB_SETUP_RECIPIENT_ENDPOINT  = 0x02
USB_SETUP_RECIPIENT_OTHER     = 0x03
USB_SETUP_SET_STAND_DEVICE = USB_SETUP_DIR_HOST_TO_DEVICE | \
                             USB_SETUP_TYPE_STANDARD | \
                             USB_SETUP_RECIPIENT_DEVICE

# General setup

def enable_regulator():

    return [LDI(R16, 0),
            STS(REGCR, R16)]        # enable the regulator

def enable_usb():

    return [LDI(R16, USBE),
            STS(USBCON, R16)]       # enable USB controller

def attach_usb():

    return [LDS(R16, UDCON),
            ANDI(R16, NOT_DETACH),
            STS(UDCON, R16)]

def enable_end_of_reset_interrupt():

    return [LDI(R16, EORSTE),
            STS(UDIEN, R16)]        # enable end of reset interrupt


# Interrupt routine boilerplate

def start_interrupt(name):

    return [name,
            PUSH(R0),
            PUSH(R1),
            PUSH(R2),
            PUSH(R3),
            PUSH(R17),
            PUSH(R16),              # Push R16 and the status register onto the stack.
            IN(R16, SREG),
            PUSH(R16)]

def end_interrupt(name):

    return [name,
            POP(R16),               # Restore the status register and R16.
            OUT(SREG, R16),
            POP(R16),
            POP(R17),
            PUSH(R3),
            PUSH(R2),
            PUSH(R1),
            PUSH(R0),
            RETI()]

# USB interrupt bit handling (UDINT or UEINTX)

def is_interrupt_bit_clear(register, bit_number, else_=""):

    return join([
        [LDS(R16, register),        # read the register
         SBRC(R16, bit_number)],    # skip the next instruction if
        goto(else_)                 # the bit is clear
        ])

def is_interrupt_bit_set(register, bit_number, else_=""):

    return join([
        [LDS(R16, register),
         SBRS(R16, bit_number)],    # continue if the bit is set
        goto(else_)                 # else jump to the label
        ])

def clear_interrupt_bit(register, bit_number):

    mask = not_dict[bit_number]
    return [ANDI(R16, mask),
            STS(register, R16)]


# Endpoint configuration

def is_endpoint_disabled(else_=""):

    return join([
        [LDS(R16, UECONX),
         SBRC(R16, EPEN_bit)],  # skip next instruction if endpoint is disabled
        goto(else_)
        ])

def enable_endpoint():

    return [LDS(R16, UECONX),
            ORI(R16, EPEN),
            STS(UECONX, R16)]

def select_endpoint(number):

    return [LDI(R16, number),
            STS(UENUM, R16)]

def configure_endpoint(number, type, direction, size, banks):

    return select_endpoint(number) + \
           enable_endpoint() + \
           [LDI(R16, type),
            ORI(R16, direction),
            STS(UECFG0X, R16)] + \
            \
           [LDS(R16, UECFG1X),
            ANDI(R16, ALLOC),   # clear everything except the allocation bit
            ORI(R16, size),
            ORI(R16, banks),
            STS(UECFG1X, R16)] + \
            \
           [LDS(R16, UECFG1X),              # allocate memory
            ORI(R16, ALLOC),
            STS(UECFG1X, R16)]

def is_endpoint_configured(number, else_=""):

    return join([
        [LDS(R16, UESTA0X),
         SBRS(R16, CFGOK_bit)],     # continue if the bit is set
        goto(else_)                 # else jump to the label
        ])

def enable_endpoint_interrupt(flag):

    # Enable endpoint interrupts
    return [LDS(R16, UEIENX),
            ORI(R16, flag),
            STS(UEIENX, R16)]

def set_address():
            
    return [ANDI(R16, NOT_ADDEN),   # Clear the ADDEN bit.
            STS(UDADDR, R16)]

def enable_address():

    return [LDS(R16, UDADDR),
            ORI(R16, ADDEN),        # set the ADDEN bit of UDADDR
            STS(UDADDR, R16)]

def wait_for_in_ready(else_=""):

    return [else_,
            LDS(R16, UEINTX),
            SBRS(R16, TXINI),
            RJMP(Reference(else_))]


# Commands

def send_zero_in_command():

    return [LDS(R16, UEINTX),
            ANDI(R16, NOT_TXINI),
            STS(UEINTX, R16)]

# Miscellaneous functions

def goto(label):

    return [RJMP(Reference(label))]

def compare(register, value, else_=""):

    return [CPI(register, value),
            BRNE(Reference(else_))]

def red_led_toggle():

    return [IN(R16, PORTD),
            LDI(R17, red_LED),
            EOR(R16, R17),
            OUT(PORTD, R16)]

def blue_led_toggle():

    return [IN(R16, PORTD),
            LDI(R17, red_LED),
            EOR(R16, R17),
            OUT(PORTD, R16)]

def led_off():

    return [IN(R16, PORTD),
            ANDI(R16, 0xff - red_LED - blue_LED),
            OUT(PORTD, R16)]


init = join([
    ["init"],

    #enable_regulator(),
    enable_usb(),
    attach_usb(),
    enable_end_of_reset_interrupt(),

    [LDI(R16, LED_enable),
     OUT(DDRD, R16)],

    led_off(),

    [LDI(R16, 255),
     STS(0x400, R16),
     LDI(R17, 255),
     STS(0x401, R17),
     SEI()]
    ])

loop = ["loop"] + goto("loop")

"""
loop = ["loop",
        LDS(R16, 0x400),
        DEC(R16),
        STS(0x400, R16),
        CPI(R16, 255),
        BRNE(Reference("loop")),

        LDS(R16, 0x401),
        DEC(R16),
        STS(0x401, R16),
        CPI(R16, 255),
        BRNE(Reference("loop")),

        IN(R16, PORTD),
        LDI(R17, red_LED),
        EOR(R16, R17),
        OUT(PORTD, R16),

        LDI(R16, 255),
        LDI(R17, 255)] + goto("loop")
"""

def usb_general():

    return join([
        start_interrupt("usb_general"),

        # Check end of reset.
        is_interrupt_bit_set(UDINT, EORSTI_bit, else_="usb_check_setup"),

        # Clear the end of reset bit.
        clear_interrupt_bit(UDINT, EORSTI_bit),

        # Select endpoint 0 (the control endpoint).
        is_endpoint_disabled(else_="usb_exit"),

        # Configure endpoint 0 as the control endpoint.
        configure_endpoint(0, TYPE_CONTROL, DIRECTION_OUT, SIZE_32, ONE_BANK),

        is_endpoint_configured(0, else_="usb_exit"),

        #configure_endpoint(1, TYPE_CONTROL, DIRECTION_OUT, SIZE_32, ONE_BANK),
        #is_endpoint_configured(0, else_="usb_exit"),

        # Set the Received Setup Interrupt Enable Flag.
        enable_endpoint_interrupt(RXSTPE),

        goto("usb_exit"),

        ["usb_check_setup"],

        #select_endpoint(0),

        # Check for a SETUP request.
        is_interrupt_bit_set(UEINTX, RXSTPI_bit, else_="usb_exit"),

        # Read the request type and the request.
        [LDS(R16, UEDATX),  # type
         LDS(R17, UEDATX)], # request

        compare(R16, USB_SETUP_SET_STAND_DEVICE, else_="usb_exit"),

        compare(R17, SETUP_GET_DESCRIPTOR, else_="check_set_address"),

            blue_led_toggle(),

            [LDS(R0, UEDATX),   # wValue (low): descriptor index
             LDS(R1, UEDATX)],  # wValue (high): descriptor type

            [LDS(R16, UEDATX),
             LDS(R17, UEDATX)], # wIndex (ignored)

            [LDS(R2, UEDATX),
             LDS(R3, UEDATX)], # wLength (maximum allowed length to return)

            # Clear the Received SETUP bit (acknowledge the request).
            clear_interrupt_bit(UDINT, RXSTPI_bit),

            goto("usb_exit"),

        ["check_set_address"],
        compare(R17, SETUP_SET_ADDRESS, else_="usb_exit"),
        
            [LDS(R16, UEDATX)],
            set_address(),
            send_zero_in_command(),
            wait_for_in_ready(else_="usb_set_address_wait"),
            enable_address(),

            # Clear the Received SETUP bit (acknowledge the request).
            clear_interrupt_bit(UDINT, RXSTPI_bit),

        end_interrupt("usb_exit")
        ])

main = ["main"] + init + loop
routines = usb_general()

a = Assembler()

program = vector_table.generate("atmega32u2") + main + routines

assembly = []

for address, opcode in a.assemble(0, program):
    print hex(address), opcode.text(), Lookup.lookup(opcode.value)
    assembly.append((address, opcode))

f = open("usb_connect.hex", "w")
for line in a.write_hex(assembly):
    f.write(line)

f.close()
