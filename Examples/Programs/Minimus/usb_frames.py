#!/usr/bin/env python

"""
usb_connect.py - Example of assembler usage.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *
from ATmega.memory import *

R0 = Register(0)
R1 = Register(1)
R16 = Register(16)
R17 = Register(17)
R28 = Register(28)
R29 = Register(29)
R30 = Register(30)
R31 = Register(31)
X = X()
Y = Y()
Z = Z()

def setY(address):
    return [LDI(R28, address & 0xff),
            LDI(R29, address >> 8)]

def setZ(address):
    return [LDI(R30, address & 0xff),
            LDI(R31, address >> 8)]

# See section 11 of the ATmega32U2 datasheet.

vector_table = VectorTable()
vector_table.add_routine("RESET", Reference("init"))
vector_table.add_routine("USB GENERAL", Reference("usb_general"))

DDRD = 0x0a
DDD5 = 0x20 # 00100000
DDD6 = 0x40 # 01000000
LED_enable = DDD5 | DDD6

PORTD = 0x0b
blue_LED = 0x40
red_LED = 0x20

REGCR = 0x0063

USBCON = 0x00d8
USBE = 0x80

UDCON = 0x00e0
DETACH = 0x01
NOT_DETACH = 0xfe

# Device interrupt register - indicates which interrupt is occurring
UDINT = 0x00e1
EORSTI = 0x08
SOFI = 0x04
EORSTI_bit = 3
SOFI_bit = 2
NOT_EORSTI = 0xf7
NOT_SOFI = 0xfb

# Device interrupt enable register - used to enable/disable interrupts
UDIEN = 0x00e2
EORSTE = 0x08
SOFE = 0x04

UDADDR = 0x00e3
ADDEN = 0x80
NOT_ADDEN = 0x7f

UEIENX = 0x00f0
RXSTPE = 0x08

SREG = 0x3f

init = [
    "init",

    LDI(R16, USBE),
    STS(USBCON, R16),       # enable USB controller

    LDS(R16, UDCON),
    ANDI(R16, NOT_DETACH),
    STS(UDCON, R16),        # attach USB

    LDI(R16, SOFE),
    STS(UDIEN, R16),        # enable start of frame interrupt

    LDI(R16, DDD6),
    OUT(DDRD, R16),         # enable LEDs

    LDI(R16, 0),
    OUT(PORTD, R16),        # turn the red LED off

    LDI(R16, 0),
    STS(0x400, R16),
    LDI(R17, 0),
    STS(0x401, R16),

    SEI()
]

loop = ["loop",
        RJMP(Reference("loop"))
    ]

usb_general = [
    "usb_general",
    PUSH(R16),              # Push R16 and the status register onto the stack.
    IN(R16, SREG),
    PUSH(R16),

    LDS(R16, UDINT),        # read the UDINT register
    SBRS(R16, SOFI_bit),    # continue if the start of frame bit is set
    RJMP(Reference("usb_exit")),
    
    ANDI(R16, NOT_SOFI),    # clear the end of reset bit
    STS(UDINT, R16),

    LDS(R16, 0x400),
    INC(R16),
    STS(0x400, R16),
    CPI(R16, 0),
    BRNE(Reference("usb_exit")),

    LDS(R17, 0x401),
    INC(R17),
    STS(0x401, R17),
    CPI(R17, 0),
    BRNE(Reference("usb_exit")),

    IN(R16, PORTD),
    LDI(R17, red_LED),
    EOR(R16, R17),
    OUT(PORTD, R16),

    "usb_exit",
    POP(R16),               # Restore the status register and R16.
    OUT(SREG, R16),
    POP(R16),
    RETI(),
]

main = ["main"] + init + loop
routines = usb_general

a = Assembler()

program = vector_table.generate("atmega32u2") + main + routines

assembly = []

for address, opcode in a.assemble(0, program):
    print hex(address), opcode.text(), Lookup.lookup(opcode.value)
    assembly.append((address, opcode))

f = open("usb_frames.hex", "w")
for line in a.write_hex(assembly):
    f.write(line)

f.close()
