#!/usr/bin/env python

"""
blink_timer.py - Example of assembler usage.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# Version of the blink program that initialises the vector table to call the
# main function on reset. This verifies that the vector table can be
# initialised and used as expected.

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *
from ATmega.memory import *

R0 = Register(0)
R16 = Register(16)
R17 = Register(17)
R30 = Register(30)
R31 = Register(31)
X = X()
Y = Y()
Z = Z()

# See section 11 of the ATmega32U2 datasheet.

vector_table = VectorTable()
vector_table.add_routine("RESET", Reference("main"))
vector_table.add_routine("TIMER0 OVF", Reference("flipPin"))

initCounter = [
    LDI(R16, 0),
    STS(0x400, R16)
    ]

DDRD = 0x0a
red_LED_enable = 0x40
blue_LED_enable = 0x20
LED_enable = red_LED_enable | blue_LED_enable

PORTD = 0x0b
blue_LED = 0x40
red_LED = 0x20

initPins = [
    IN(R0, DDRD),           # R0 = (DDRD)
    LDI(R16, LED_enable),
    OR(R0, R16),            # R0 |= LED_enable
    OUT(DDRD, R0),          # (DDRD) = R0
    
    LDI(R16, blue_LED),     # turn on the blue LED
    OUT(PORTD, R16)
    ]

TCCR0A = 0x24
TCCR0B = 0x25

WGM00 = 0x01
WGM01 = 0x02

TOIE0 = 0x01
TIMSK0 = 0x006e

enableInterrupts = [
    
    LDI(R16, WGM01 | WGM00),    # set WGM00 and WGM01 to select Fast PWM mode
                                # (section 15.9.1)
    OUT(TCCR0A, R16),           # store TCCR0A
    OUT(TCCR0B, R16),           # store TCCR0B

    LDI(R16, TOIE0),        # set bit 0 (TOIE0 of TIMSK0) to enable overflow
                            # interrupts for timer/counter0 (page 107)
    LDI(R30, TIMSK0 & 0xff),
    LDI(R31, TIMSK0 >> 8),
    STDzi(Z, R16),

    SEI(),
    ]

flipPin = [
    "flipPin",
    LDS(R0, 0x400),
    INC(R0),
    STS(0x400, R0),
    BRNE(Reference("exit flip")),
    
    IN(R17, PORTD),
    LDI(R16, blue_LED | red_LED),
    EOR(R17, R16),
    OUT(PORTD, R17), # (PORTD) = (PORTD) ^ 0x60 (toggle LEDs)
    "exit flip",
    RETI(),
    ]

loop = ["loop",
        RJMP(Reference("loop"))
    ]

main = ["main"] + [CLI()] + initCounter + initPins + enableInterrupts + loop

a = Assembler()

program = vector_table.generate("atmega32u2") + main + flipPin

assembly = []

for address, opcode in a.assemble(0, program):
    print hex(address), opcode.text(), Lookup.lookup(opcode.value)
    assembly.append((address, opcode))

f = open("blink.hex", "w")
for line in a.write_hex(assembly):
    f.write(line)

f.close()
