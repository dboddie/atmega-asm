#!/usr/bin/env python

"""
led_button.py - Example of assembler usage.

Copyright (C) 2013 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# Version of the blink program that initialises the vector table to call the
# main function on reset. This verifies that the vector table can be
# initialised and used as expected.

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *
from ATmega.memory import *

R0 = Register(0)
R16 = Register(16)
R17 = Register(17)
R30 = Register(30)
R31 = Register(31)
X = X()
Y = Y()
Z = Z()

# See section 11 of the ATmega32U2 datasheet.

vector_table = VectorTable()
vector_table.add_routine("RESET", Reference("main"))

DDRD = 0x0a
red_LED_enable = 0x40
blue_LED_enable = 0x20
LED_enable = red_LED_enable | blue_LED_enable

PORTD = 0x0b
red_LED = 0x40
blue_LED = 0x20

PIND = 0x9
button_pressed = 0x80

main = [
    "main",
    
    LDI(R16, LED_enable),
    OUT(DDRD, R16),

    "loop",
    IN(R16, PIND),
    ANDI(R16, button_pressed),
    CPI(R16, 0),                    # DDD7 = 0 if pressed
    BRNE(Reference("not_pressed")),

    LDI(R16, red_LED),
    OUT(DDRD, R16),
    RJMP(Reference("loop")),

    "not_pressed",
    LDI(R16, blue_LED),
    OUT(DDRD, R16),
    RJMP(Reference("loop"))
    ]

a = Assembler()

program = vector_table.generate("atmega32u2") + main

assembly = []

for address, opcode in a.assemble(0, program):
    print hex(address), opcode.text(), Lookup.lookup(opcode.value)
    assembly.append((address, opcode))

f = open("led_button.hex", "w")
for line in a.write_hex(assembly):
    f.write(line)

f.close()
