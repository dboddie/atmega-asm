#!/usr/bin/env python

"""
assembly.py - Example of assembler usage.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from ATmega.definitions import *
from ATmega.assembler import Assembler
from ATmega.instructions import *

# Define short names for the register we want to use.
R16 = Register(16)

# Define the sequence of instructions used in the program.
program = [LDI(R16, 255),           # R16 = 255
           "loop",                  # loop:
           SUBI(R16, 1),            #     R16 -= 1
           BRGE(Reference("loop"))] #     if >= 0 then loop

print
print "Initial program:"

for ins in program:
    print ins

print

# Create an Assembler to convert the instructions into opcodes
# and create a list to hold those opcodes.
asm = Assembler()
assembled = []

print "Address ", "Opcode ", "Instruction"

# Assemble the program at address 0x100.
# Each instruction assembled yields its address and an opcode.

for address, opcode in asm.assemble(0x100, program):

    # Print these out, mapping back to the original instruction as
    # a check.
    print "%04x     %s    %s" % (address, opcode.text(), Lookup.lookup(opcode.value))
    
    # Append the address and the opcode to the assembled list of opcodes.
    assembled.append((address, opcode))

print
print "HEX output:"

# Convert the opcodes into HEX format for sending to a device.

lines = ""
for line in asm.write_hex(assembled):
    lines += line

# Print them all at once to avoid inserting newlines into the
# output. It is more convenient to write them directly to a
# file object.
print lines
