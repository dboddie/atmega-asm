#!/usr/bin/env python

"""
disassembly.py - Example of disassembler usage.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from ATmega.definitions import *
from ATmega.disassembler import Bytes, Disassembler
from ATmega.instructions import *

# Define some input code in binary format.
input_code = [0x0f, 0xef, 0x01, 0x50, 0xf4, 0xf7]

# Define some input code in HEX format.

input_hex = """:060100000FEF0150F4F7BF\r
:00000001FF\r
"""

# Create a disassembler to work with.
dis = Disassembler()

print
print "From binary data:"
print "Address ", "Opcode ", "Instruction"

# Disassemble the program supplied as a sequence of bytes.
# Each opcode decoded yields an address, opcode string and a list of
# instructions that correspond to the opcode.

for address, opcode_string, instruction in dis.disassemble(0x100, input_code):

    if len(instruction) == 1:
        print "%04x     %s    %s" % (address, opcode_string, instruction[0])
    else:
        print "%04x     %s    %s ; %s" % (address, opcode_string, instruction[0], instruction[1:])

print
print "From HEX data:"
print "Address ", "Opcode ", "Instruction"

# Disassemble the program supplied in HEX format.

for address, opcode_string, instruction in dis.disassemble_hex(input_hex):

    if len(instruction) == 1:
        print "%04x     %s    %s" % (address, opcode_string, instruction[0])
    else:
        print "%04x     %s    %s ; %s" % (address, opcode_string, instruction[0], instruction[1:])
