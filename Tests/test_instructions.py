#!/usr/bin/env python

"""
test_instructions.py - Perform basic checks on the ATmega package.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from ATmega import instructions

def find_instructions():

    # Find and return the Instruction subclasses in the instructions module.
    
    ins = []
    
    for obj in instructions.__dict__.values():
    
        if type(obj) == type(instructions.Instruction) and \
            obj != instructions.Instruction and \
            issubclass(obj, instructions.Instruction):
        
            ins.append(obj)
    
    print len(ins), "opcodes"
    return ins

def test_opcode_definitions(ins):

    # Collect lists of instructions with the same opcode definitions, checking
    # that only specific instructions have the same opcodes.
    
    opcodes_dict = {}
    
    for obj in ins:
    
        if type(obj) == type(instructions.Instruction) and \
            obj != instructions.Instruction and \
            issubclass(obj, instructions.Instruction):
        
            opcodes_dict.setdefault(obj.opcode.text, []).append(obj.text)
    
    for text, instruction_list in opcodes_dict.items():
    
        # Skip opcodes that are known to represent multiple instructions.
        # We should perhaps check that the list contains the instructions we expect
        # for each of these.
        
        if len(instruction_list) > 1:
            if text == instructions.ANDI.opcode.text and len(instruction_list) == 2:
                # ANDI, CBR
                pass
            elif text == instructions.ORI.opcode.text and len(instruction_list) == 2:
                # ORI, SBR
                pass
            elif text == instructions.BRLO.opcode.text and len(instruction_list) == 2:
                # BRCS, BRLO
                pass
            elif text == instructions.SPM.opcode.text and len(instruction_list) == 2:
                # SPM, SPM2
                pass
            elif text == instructions.BRCC.opcode.text and len(instruction_list) == 2:
                # BRCC, BRSH
                pass
            else:
                print "Opcode", text, "used in multiple instructions:", instruction_list

def test_syntaxes(ins):

    for instruction in ins:
    
        # Extract the parameter names expected by the instruction.
        
        parameters = set()
        for parameter in instruction.syntax.parameters:
        
            if not isinstance(parameter, str):
                parameters.add(parameter.name)
        
        # For each field required in an opcode, check that there is a parameter
        # name defined in the instruction syntax.
        
        for data_type, mask, shift in instruction.opcode.template:
        
            if isinstance(data_type, str):
                if data_type not in parameters:
                    print instruction.text+":", data_type, "not covered by syntax parameters", parameters

def test_opcode_values():

    m = []
    i = 0
    while i < 256:
    
        j = 0
        line = []
        while j < 256:
        
            try:
                ins = instructions.Lookup.lookup(i * 256 + j)
                line.append(ins)
            except LookupError:
                line.append(None)
            
            j += 1
        
        m.append(line)
        i += 1
    
    return m


if __name__ == "__main__":

    ins = find_instructions()
    test_opcode_definitions(ins)
    test_syntaxes(ins)
    #test_opcode_values()
