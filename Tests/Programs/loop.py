#!/usr/bin/env python

from ATmega.definitions import *
from ATmega.assembler import Assembler
from ATmega.instructions import *

R16 = Register(16)

program = [
    LDI(R16, 123),
    SBCI(R16, 1),
    BRCC(-4),
    SLEEP()
    ]

a = Assembler()
for address, opcode in a.assemble(0xa00, program):

    print hex(address), opcode
