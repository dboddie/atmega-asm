#!/usr/bin/env python

# Version of the blink program that initialises the vector table to call the
# main function on reset. This verifies that the vector table can be
# initialised and used as expected.

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *

R0 = Register(0)
R16 = Register(16)
R17 = Register(17)
R18 = Register(18)
R19 = Register(19)
R30 = Register(30)
R31 = Register(31)
X = X()
Y = Y()
Z = Z()

# According to the datasheet, "Each Interrupt Vector occupies two instruction
# words in ATmega168..."

interrupt_table = [
    JMP(Reference("main")), # 0x0000 (RESET)
    RETI(),                 # 0x0004 (INT0)
    RETI(),
    RETI(),                 # 0x0008 (INT1)
    RETI(),
    RETI(),                 # 0x000c (PCINT0)
    RETI(),
    RETI(),                 # 0x0010 (PCINT1)
    RETI(),
    RETI(),                 # 0x0014 (PCINT2)
    RETI(),
    RETI(),                 # 0x0018 (WDT)
    RETI(),
    RETI(),                 # 0x001c (TIMER2 COMPA)
    RETI(),
    RETI(),                 # 0x0020 (TIMER2 COMPB)
    RETI(),
    RETI(),                 # 0x0024 (TIMER2 OVF)
    RETI(),
    RETI(),                 # 0x0028 (TIMER1 CAPT)
    RETI(),
    RETI(),                 # 0x002c (TIMER1 COMPA)
    RETI(),
    RETI(),                 # 0x0030 (TIMER1 COMPB)
    RETI(),
    RETI(),                 # 0x0034 (TIMER1 OVF)
    RETI(),
    RETI(),                 # 0x0038 (TIMER0 COMPA)
    RETI(),
    RETI(),                 # 0x003c (TIMER0 COMPB)
    RETI(),
    RJMP(Reference("flipPin")),# 0x0040 (TIMER0 OVF)
    RETI(),
    RETI(),                 # 0x0044 (SPI. STC)
    RETI(),
    RETI(),                 # 0x0048 (USART. RX)
    RETI(),
    RETI(),                 # 0x004c (USART. UDRE)
    RETI(),
    RETI(),                 # 0x0050 (USART. TX)
    RETI(),
    RETI(),                 # 0x0054 (ADC)
    RETI(),
    RETI(),                 # 0x0058 (EE READY)
    RETI(),
    RETI(),                 # 0x005c (ANALOG COMP)
    RETI(),
    RETI(),                 # 0x0060 (TWI)
    RETI(),
    RETI(),                 # 0x0064 (SPM READY)
    RETI(),
    ]

initCounter = [
    LDI(R16, 0),
    STS(0x100, R16)
    ]

initPin = [
    IN(R0, 0x04),   # R0 = (DDRB)
    LDI(R16, 32),
    OR(R0, R16),    # R0 |= DDB5 (pin 13 writable)
    OUT(0x04, R0),  # (DDRB) = R0
    ]

enableInterrupts = [
    LDI(R16, 0x03),  # set WGM00 and WGM01 to select Fast PWM mode (page 102)
    OUT(0x24, R16),  # store TCCR0A
    OUT(0x25, R16),  # store TCCR0B

    LDI(R16, 0x01), # set bit 0 (TOIE0 of TIMSK0) to enable overflow interrupts
                    # for timer/counter0 (page 105)
    LDI(R30, 0x6E),
    LDI(R31, 0x00),
    STDzi(Z, R16),

    SEI(),
    ]

flipPin = [
    "flipPin",
    LDS(R0, 0x100),
    INC(R0),
    STS(0x100, R0),
    BRNE(Reference("exit flip")),
    
    IN(R17, 0x05),
    LDI(R16, 32),   # R16 = PORTB5
    EOR(R17, R16),
    OUT(0x05, R17), # (PORTB) = (PORTB) ^ 32 (pin 13 on/off)
    "exit flip",
    RETI(),
    ]

loop = ["loop",
        RJMP(Reference("loop"))
    ]

main = ["main"] + initCounter + initPin + enableInterrupts + loop

a = Assembler()

program = interrupt_table + main + flipPin

assembly = []

for address, opcode in a.assemble(0, program):
    print hex(address), opcode.text(), Lookup.lookup(opcode.value)
    assembly.append((address, opcode))

f = open("blink.hex", "w")
for line in a.write_hex(assembly):
    f.write(line)

f.close()
