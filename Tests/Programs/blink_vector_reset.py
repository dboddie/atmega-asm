#!/usr/bin/env python

# Version of the blink program that initialises the vector table to call the
# main function on reset. This verifies that the vector table can be
# initialised and used as expected.

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *

R0 = Register(0)
R16 = Register(16)
R17 = Register(17)
R18 = Register(18)
R19 = Register(19)
X = X()
Y = Y()
Z = Z()

# According to the datasheet, "Each Interrupt Vector occupies two instruction
# words in ATmega168..."

interrupt_table = [
    JMP(Reference("main")), # 0x0000 (RESET)
    RETI(),                 # 0x0004 (INT0)
    RETI(),
    RETI(),                 # 0x0008 (INT1)
    RETI(),
    RETI(),                 # 0x000c (PCINT0)
    RETI(),
    RETI(),                 # 0x0010 (PCINT1)
    RETI(),
    RETI(),                 # 0x0014 (PCINT2)
    RETI(),
    RETI(),                 # 0x0018 (WDT)
    RETI(),
    RETI(),                 # 0x001c (TIMER2 COMPA)
    RETI(),
    RETI(),                 # 0x0020 (TIMER2 COMPB)
    RETI(),
    RETI(),                 # 0x0024 (TIMER2 OVF)
    RETI(),
    RETI(),                 # 0x0028 (TIMER1 CAPT)
    RETI(),
    RETI(),                 # 0x002c (TIMER1 COMPA)
    RETI(),
    RETI(),                 # 0x0030 (TIMER1 COMPB)
    RETI(),
    RETI(),                 # 0x0034 (TIMER1 OVF)
    RETI(),
    RETI(),                 # 0x0038 (TIMER0 COMPA)
    RETI(),
    RETI(),                 # 0x003c (TIMER0 COMPB)
    RETI(),
    RETI(),                 # 0x0040 (TIMER0 OVF)
    RETI(),
    RETI(),                 # 0x0044 (SPI. STC)
    RETI(),
    RETI(),                 # 0x0048 (USART. RX)
    RETI(),
    RETI(),                 # 0x004c (USART. UDRE)
    RETI(),
    RETI(),                 # 0x0050 (USART. TX)
    RETI(),
    RETI(),                 # 0x0054 (ADC)
    RETI(),
    RETI(),                 # 0x0058 (EE READY)
    RETI(),
    RETI(),                 # 0x005c (ANALOG COMP)
    RETI(),
    RETI(),                 # 0x0060 (TWI)
    RETI(),
    RETI(),                 # 0x0064 (SPM READY)
    RETI(),
    ]

enableInterrupts = [
    SEI()
    ]

initPin = [
    IN(R0, 0x04),   # R0 = (DDRB)
    LDI(R16, 32),
    OR(R0, R16),    # R0 |= DDB5 (pin 13 writable)
    OUT(0x04, R0),  # (DDRB) = R0
    ]

setPin = [
    "setPin",
    LDI(R16, 32),   # R0 = PORTB5
    OUT(0x05, R16),  # (PORTB) = 32 (only pin 13 on)
    RET(),
    ]

clearPin = [
    "clearPin",
    LDI(R16, 0),    # R16 = 0
    OUT(0x05, R16),  # (PORTB) = 0 (all pins off)
    RET()
    ]

wait = [
    "wait",
    LDI(R18, 15),
    SER(R17),
    SER(R16),
    "wait_loop",
    SUBI(R16, 1),
    SBCI(R17, 0),
    SBCI(R18, 0),
    BRCC(Reference("wait_loop")),
    RET()
    ]

loop = ["loop",
        RCALL(Reference("setPin")),
        RCALL(Reference("wait")),
        RCALL(Reference("clearPin")),
        RCALL(Reference("wait")),
        RJMP(Reference("loop"))
    ]

sleep = [
    SLEEP()
    ]

main = ["main"] + enableInterrupts + initPin + loop + sleep

a = Assembler()

program = interrupt_table + main + wait + clearPin + setPin

assembly = []

for address, opcode in a.assemble(0, program):
    print hex(address), opcode.text(), Lookup.lookup(opcode.value)
    assembly.append((address, opcode))

f = open("blink.hex", "w")
for line in a.write_hex(assembly):
    f.write(line)

f.close()
