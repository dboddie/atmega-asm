#!/usr/bin/env python

import sys

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *

R16 = Register(16)

program = [
    LDI(R16, 123),
    "loop",
    SBCI(R16, 1),
    BRSH(Reference("loop")),
    SLEEP()
    ]

a = Assembler()

for record in a.write_hex(a.assemble(0, program)):

    print record,
