#!/usr/bin/env python

# Transcription of generated code from the Blink sketch supplied with the
# Arduino development environment. Not tested or used for generating programs
# for the board.

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *

R0 = Register(0)
R1 = Register(1)
R14 = Register(14)
R15 = Register(15)
R16 = Register(16)
R17 = Register(17)
R18 = Register(18)
R19 = Register(19)
R20 = Register(20)
R21 = Register(21)
R22 = Register(22)
R23 = Register(23)
R24 = Register(24)
R25 = Register(25)
R26 = Register(26)
R27 = Register(27)
R28 = Register(28)
R29 = Register(29)
R30 = Register(30)
R31 = Register(31)
X = X()
Z = Z()

__init = [
    EOR(R1, R1),
    OUT(0x3f, R1),
    LDI(R28, 0xff),
    LDI(R29, 0x04),
    OUT(0x3e, R29),
    OUT(0x3d, R28)
    ]

__do_copy_data = [
    LDI(R17, 0x01),
    LDI(R26, 0x00),
    LDI(R27, 0x01),
    LDI(R30, 0x5c),
    LDI(R31, 0x04),
    RJMP(Reference("do_copy_data_start"))
    ]

do_copy_data_loop = [
    "do_copy_data_loop",
    LPMiii(R0, Z),
    STii(X, R0)
    ]

do_copy_data_start = [
    "do_copy_data_start",
    CPI(R26, 0X02),
    CPC(R27, R17),
    BRNE(Reference("do_copy_data_loop"))
    ]


millis = [
    IN(R18, 0x3f),
    CLI(),
    LDS(R22, 0),
    LDS(R23, 0),
    LDS(R24, 0),
    LDS(R25, 0),
    OUT(0x3f, R18),
    RET()
    ]

init = [
    SEI(),
    IN(R24, 0x24),
    ORI(R24, 0x02),
    OUT(0x24, R24), # I/O 0x24 = 0x24 | 0x02
    IN(R24, 0x24),
    ORI(R24, 0x01),
    OUT(0x24, R24), # I/O 0x24 = 0x24 | 0x01 (enable WGM01 and WGM00 in TCCR0A)
    IN(R24, 0x25),
    ORI(R24, 0x02),
    OUT(0x25, R24),
    IN(R24, 0x25),
    ORI(R24, 0x01),
    OUT(0x25, R24), # I/O 0x25 = 0x25 | 0x03 (enable CS01 and CS00 in TCCR0B)
    LDI(R30, 0x6E),
    LDI(R31, 0x00),
    LDDzi(R24, Z),
    ORI(R24, 0x01), # I/O (0x6E) = (0x6E) | 0x01 (enable timer/counter 0 interrupts)
    STDzi(Z, R24),
    LDI(R30, 0x81),
    LDI(R31, 0x00),
    LDDzi(R24, Z),
    ORI(R24, 0x02),
    STDzi(Z, R24),
    LDDzi(R24, Z),
    ORI(R24, 0x01),
    STDzi(Z, R24),
    LDI(R30, 0x80),
    LDI(R31, 0x00),
    LDDzi(R24, Z),
    ORI(R24, 0x01),
    STDzi(Z, R24),
    LDI(R30, 0xB1),
    LDI(R31, 0x00),
    LDDzi(R24, Z),
    ORI(R24, 0x04),
    STDzi(Z, R24),
    LDI(R30, 0xB0),
    LDI(R31, 0x00),
    LDDzi(R24, Z),
    ORI(R24, 0x01),
    STDzi(Z, R24),
    LDI(R30, 0x7A),
    LDI(R31, 0x00),
    LDDzi(R24, Z),
    ORI(R24, 0x04),
    STDzi(Z, R24),
    LDDzi(R24, Z),
    ORI(R24, 0x02),
    STDzi(Z, R24),
    LDDzi(R24, Z),
    ORI(R24, 0x01),
    STDzi(Z, R24),
    LDDzi(R24, Z),
    ORI(R24, 0x80),
    STDzi(Z, R24),
    STS(0x00C1, R1),
    RET()
    ]

delay = [
    "delay",
    PUSH(R14),
    PUSH(R15),
    PUSH(R16),
    PUSH(R17),
    MOVW(R15, R14, R23, R22),
    MOVW(R17, R16, R25, R24),
    IN(R24, 0x3f),      # read timer
    CLI(),
    LDS(R20, 0x0106),
    LDS(R21, 0x0107),
    LDS(R22, 0x0108),
    LDS(R23, 0x0109),
    OUT(0x3f, R24),
    "delay_loop",
    IN(R18, 0x3f),
    CLI(),
    LDS(R24, 0x0106),
    LDS(R25, 0x0107),
    LDS(R26, 0x0108),
    LDS(R27, 0x0109),
    OUT(0x3f, R18),
    SUB(R24, R20),
    SBC(R25, R21),
    SBC(R26, R22),
    SBC(R27, R23),
    CP(R24, R14),
    CPC(R25, R15),
    CPC(R26, R16),
    CPC(R27, R17),
    BRCS(Reference("delay_loop")),
    POP(R17),
    POP(R16),
    POP(R15),
    POP(R14),
    RET()
    ]

pinMode = [
    "pinMode",
    MOV(R18, R24),
    EOR(R19, R19),
    MOVW(R25, R24, R19, R18),     # R24,R25 = R18,R19
    SUBI(R24, 0x75),
    SBCI(R25, 0xff),
    MOVW(R31, R30, R25, R24),     # R30,R31 = R24,R25
    LPMii(R25, Z),                # load value from R30,R31
    SUBI(R18, 0x89),
    SBCI(R19, 0xff),
    MOVW(R31, R30, R19, R18),
    LPMii(R24, Z),                # load value from R30,R31
    AND(R24, R24),
    BREQ(Reference("pinMode_exit")),
    MOV(R30, R24),
    EOR(R31, R31),
    SUBI(R30, 0x98),
    SBCI(R31, 0xff),
    LPMii(R30, Z),      # load the port number
    EOR(R31, R31),      # zero R31
    AND(R22, R22),
    BRNE(Reference("pinMode_else")),
    LDDzi(R24, Z),      # load value from R30,R31
    COM(R25),
    AND(R24, R25),      # clear a bit
    STDzi(Z, R24),      # store the value to R30,R31
    RET(),
    "pinMode_else",
    LDDzi(R24, Z),      # load value from R30,R31
    OR(R24, R25),       # set a bit
    STDzi(Z, R24),      # store value to R30,R31
    "pinMode_exit",
    RET()
    ]

setPinMode = [
    IN(R16, 0x04),   # R0 = (DDRB)
    ORI(R16, 32),    # R0 |= DDB5 (pin 13 writable)
    OUT(0x04, R16),  # (DDRB) = R0
    ]



a = Assembler()

program = init + setPinMode

for address, opcode in a.assemble(0, program):
    print hex(address), opcode.text(), Lookup.lookup(opcode.value)
