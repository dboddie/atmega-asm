#!/usr/bin/env python

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *

R0 = Register(0)
R16 = Register(16)
R17 = Register(17)
R18 = Register(18)
R19 = Register(19)
X = X()
Y = Y()
Z = Z()

initPin = [
    IN(R0, 0x04),   # R0 = (DDRB)
    LDI(R16, 32),
    OR(R0, R16),    # R0 |= DDB5 (pin 13 writable)
    OUT(0x04, R0),  # (DDRB) = R0
    ]

setPin = [
    LDI(R16, 32),   # R0 = PORTB5
    OUT(0x05, R16),  # (PORTB) = 32 (only pin 13 on)
    ]

clearPin = [
    LDI(R16, 0),    # R16 = 0
    OUT(0x05, R16),  # (PORTB) = 0 (all pins off)
    ]

sleep = [
    SLEEP()
    ]

a = Assembler()

program = initPin + setPin + sleep

assembly = []

for address, opcode in a.assemble(0, program):
    print hex(address), opcode.text(), Lookup.lookup(opcode.value)
    assembly.append((address, opcode))

f = open("led_on.hex", "w")
for line in a.write_hex(assembly):
    f.write(line)

f.close()
