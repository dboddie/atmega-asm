#!/usr/bin/env python

# Subroutine of the blink program where all operations are called as
# subroutines. The duration of the wait subroutine is shorter than that used in
# the inline version.

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *

R0 = Register(0)
R16 = Register(16)
R17 = Register(17)
R18 = Register(18)
R19 = Register(19)
X = X()
Y = Y()
Z = Z()

initPin = [
    IN(R0, 0x04),   # R0 = (DDRB)
    LDI(R16, 32),
    OR(R0, R16),    # R0 |= DDB5 (pin 13 writable)
    OUT(0x04, R0),  # (DDRB) = R0
    ]

setPin = [
    "setPin",
    LDI(R16, 32),   # R0 = PORTB5
    OUT(0x05, R16),  # (PORTB) = 32 (only pin 13 on)
    RET(),
    ]

clearPin = [
    "clearPin",
    LDI(R16, 0),    # R16 = 0
    OUT(0x05, R16),  # (PORTB) = 0 (all pins off)
    RET()
    ]

wait = [
    "wait",
    LDI(R18, 31),
    SER(R17),
    SER(R16),
    "wait_loop",
    SUBI(R16, 1),
    SBCI(R17, 0),
    SBCI(R18, 0),
    BRCC(Reference("wait_loop")),
    RET()
    ]

loop = [
    "loop",
    RCALL(Reference("setPin")),
    RCALL(Reference("wait")),
    RCALL(Reference("clearPin")),
    RCALL(Reference("wait")),
    RJMP(Reference("loop"))
    ]

sleep = [
    SLEEP()
    ]

a = Assembler()

program = initPin + loop + sleep + wait + clearPin + setPin

assembly = []

for address, opcode in a.assemble(0, program):
    print hex(address), opcode.text(), Lookup.lookup(opcode.value)
    assembly.append((address, opcode))

f = open("blink.hex", "w")
for line in a.write_hex(assembly):
    f.write(line)

f.close()
