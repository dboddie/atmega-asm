#!/usr/bin/env python

from ATmega.definitions import *
from ATmega.assembler import *
from ATmega.instructions import *

R16 = Register(16)

program = [
    LDI(R16, 123),
    "loop",
    SBCI(R16, 1),
    BRSH(Reference("loop")),
    SLEEP()
    ]

a = Assembler()

for address, opcode in a.assemble(0xa00, program):

    print hex(address), opcode
